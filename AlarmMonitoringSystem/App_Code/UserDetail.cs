﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmMonitoringSystem
{
    public class UserDetail
    {
        public static string userName, loginType;
        public static long userId;

        public UserDetail(string user, string type,long id)
        {
            userName = user;
            loginType = type;
            userId = id;
        }

        public  string Username { get { return userName; } }
        public string LoginType { get { return loginType; } }
        public long UserId { get { return userId; } }

        public UserDetail() { }
    }
}
