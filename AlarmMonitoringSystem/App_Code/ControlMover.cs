using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;
using AlarmMonitoringSystem.ColdendForeman;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace AlarmMonitoringSystem
{
    class ControlMover
    {
        static int width = 160, height = 120, marginLeft = 40, marginDown = 20, startX = 30, startY = 20;
        public enum Direction
        {
            Any,
            Horizontal,
            Vertical
        }

        public static void Init(Control control)
        {

            Init(control, Direction.Any);
        }

        public static void Init(Control control, Direction direction)
        {
            Control container = control;
            if (control.GetType() == typeof(CheckBox) || control.GetType() == typeof(TextBox))
            {
                container = control.Parent;

            }
            Init(control, container, direction);
        }

        public static void Init(Control control, Control container, Direction direction)
        {
            int containerTop = 0, containerLeft = 0;

            bool Dragging = false;
            Point DragStart = Point.Empty;
            int maxHeight = 0;
            control.MouseDown += delegate (object sender, MouseEventArgs e)
            {
                containerTop = container.Top;
                containerLeft = container.Left;
                Dragging = true;
                int left = e.X, top = e.Y;
                // left = containerLeft;
                // top = containerTop;
                left = (left / (width + marginLeft)) * (width + marginLeft) + startX;
                left = (left == 0 ? startX : left);
                top = (top / (height + marginDown)) * (height + marginDown) + startY;
                top = (top == 0 ? startY : top);
                DragStart = new Point(left, top);
                control.Capture = true;
                 maxHeight =container.Parent.Height-22;
            };
            control.MouseUp += delegate (object sender, MouseEventArgs e)
            {
                Dragging = false;
                control.Capture = false;
            };

            control.MouseMove += delegate (object sender, MouseEventArgs e)
            {
                if (Dragging)
                {
                    Control parent = container.Parent;
                    //if (direction != Direction.Vertical)
                    //     container.Left = Math.Max(0,                    e.X + container.Left - DragStart.X);
                    //if (direction != Direction.Horizontal)
                    //container.Top = Math.Max(0,                    e.Y + container.Top - DragStart.Y);
                    // MessageBox.Show(container.Top.ToString() + "     " + DragStart.Y.ToString());
                    System.Threading.Thread.Sleep(1000);
                    int left = e.X, top = e.Y;

                    left = ((left / (width + marginLeft)) * (width + marginLeft)) + startX;
                    left = (left == 0 ? startX : left);
                    top = ((top / (height + marginDown)) * (height + marginDown)) + startY;
                    top = (top == 0 ? startY : top);

                    //if (Math.Max(0, top + container.Top - DragStart.Y) > parent.Height)
                   
                    //if (e.Y > parent.Height)
                    //{
                    //    container.Left = containerLeft;
                    //    container.Top = containerTop;
                    //    return;
                    //}
                    //else {
                        container.Left = Math.Max(0, left + container.Left - DragStart.X);
                        container.Top = Math.Max(0, top + container.Top - DragStart.Y);
                   // }
                   if(container.Top > maxHeight)
                    {
                        container.Left = containerLeft;
                        container.Top = containerTop;
                    }

                    //System.Threading.Thread.Sleep(1000);
                    //if (container.Top > container.Parent.Controls[container.Parent.Controls.Count - 1].Top)
                    //{
                    //    container.Top = container.Parent.Controls[container.Parent.Controls.Count - 1].Top;
                    //    container.Left = container.Parent.Controls[container.Parent.Controls.Count - 1].Left;
                    //}

                    //if (container.Left > container.Parent.Controls[container.Parent.Controls.Count - 1].Left)
                    //{
                    //    container.Top = container.Parent.Controls[container.Parent.Controls.Count - 1].Top;
                    //    container.Left = container.Parent.Controls[container.Parent.Controls.Count - 1].Left;
                    //}

                    if (container.Top != containerTop || container.Left != containerLeft)
                    {
                        foreach (Panel pnl in container.Parent.Controls)
                        {
                            if (pnl.Name != container.Name)
                            {
                                if (container.Top == pnl.Top && container.Left == pnl.Left)
                                {
                                    pnl.Top = containerTop;
                                    pnl.Left = containerLeft;
                                }

                                //if (containerTop <= pnl.Top)
                                //{
                                //    if (pnl.Top < container.Top)
                                //    {
                                //        pnl.Left = (pnl.Left == startX ? (pnl.Top == startY ? startX : ((width + marginLeft) * 2 + startX)) : (pnl.Left - (width + marginLeft)));
                                //        pnl.Top = (pnl.Top == startY ? (startY) : (pnl.Top - (height + marginDown)));
                                //    }
                                //    else if (pnl.Top == container.Top)
                                //    {
                                //        if (pnl.Left <= container.Left)
                                //        {
                                //            int prvLeft = pnl.Left;
                                //            pnl.Left = (pnl.Left == startX ? (pnl.Top == startY ? startX : ((width + marginLeft) * 2 + startX)) : (pnl.Left - (width + marginLeft)));
                                //            pnl.Top = (prvLeft == startX && pnl.Top != startY ? (pnl.Top - (height + marginDown)) : (pnl.Top == startY ? (startY) : (pnl.Top)));
                                //        }
                                //    }
                                //}

                            }

                        }
                        ReArrageOrder((GroupBox)container.Parent);


                        //if ((pnl.Left * pnl.Top <= container.Left * container.Top) && pnl.Name != container.Name)
                        //{
                        //    pnl.Left = (pnl.Left == startX ? (pnl.Top == startY ? startX : ((width + marginLeft) * 2 + startX)) : (pnl.Left - (width + marginLeft)));
                        //    pnl.Top = (pnl.Top == startY ? (startY) : (pnl.Top - (height + marginDown)));
                        //}
                    }


                }
            };
        }

        public static void ReArrageOrder(GroupBox tabPage)
        {
            DataTable tb = new DataTable();
            tb.Columns.Add(new DataColumn("DefectId", typeof(int)));
            tb.Columns.Add(new DataColumn("Order", typeof(int)));
            int order = 1;
            foreach (Panel pnl in tabPage.Controls)
            {
                //foreach (Control chk in pnl.Controls)
                //{
                //if (chk.GetType() == typeof(CheckBox))
                //{
                foreach (Control lbl in pnl.Controls)
                {
                    DataRow dr = tb.NewRow();
                    if (lbl.GetType() == typeof(Label))
                    {
                        dr[0] = ((Label)lbl).Text;
                        dr[1] = GetOrder(pnl.Left, pnl.Top);
                        tb.Rows.Add(dr);
                        order++;
                    }

                }
                //}
                //}
            }

            DefectBAL obj = new DefectBAL();
            obj.UpdateDefectOrder(tb, Defect.Id);
          //  Defect jj = new Defect(Defect.Id);
            //jj.BindDefects("Critical", tabPage1);
            //jj.BindDefects("Major", tabPage2);
            //jj.BindDefects("Minor", tabPage3);
        }

        public static int GetOrder(int left, int top)
        {
            return (((left - startX) / (width + marginLeft)) + 1) + ((((top - startY) / (height + marginDown))) * 3);
        }
    }
}