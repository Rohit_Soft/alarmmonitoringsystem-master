﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;

namespace AlarmMonitoringSystem.ColdendForeman
{
    public partial class DefectShowHide : Form
    {
        public static long Id = 0;
        DefectBAL objDefect = new DefectBAL();
        public DefectShowHide(long id)
        {
            Id = id;
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = objDefect.GetAllDefectsColdendForRemove(id);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                objDefect.UpdateDefectColdend(Convert.ToInt64(((DataGridViewTextBoxCell)dataGridView1.Rows[e.RowIndex].Cells["ColId"]).Value), Convert.ToBoolean(((DataGridViewCheckBoxCell)dataGridView1.Rows[e.RowIndex].Cells["Remove"]).Value));
                Defect obj = new Defect(Id, UserDetail.userName);
                obj.Refresh();
                // groupBox1.Focus();
              //  BindData(Convert.ToString(((DataGridViewTextBoxCell)dataGridView1.Rows[e.RowIndex].Cells["cathidden"]).Value));
            }
        }
    }
}
