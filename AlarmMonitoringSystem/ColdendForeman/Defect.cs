﻿using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmMonitoringSystem.ColdendForeman
{
    public partial class Defect : Form
    {
        public static long Id = 0;
        // public static string DefectIds = "", tempDefects = "";
        DefectBAL objDefect = new DefectBAL();
        // ControlMover mover = new ControlMover();
        public Defect(long id, string lineNumber)
        {
            Id = id;
            InitializeComponent();
            flowLayoutPanel1.BackColor = flowLayoutPanel2.BackColor = flowLayoutPanel3.BackColor = flowLayoutPanel4.BackColor = ColorTranslator.FromHtml("#F0C869");
            // this.Text = UserDetail.userName + " - " + lineNumber;

            BindData();
        }

        private void BindData()
        {
            //tempDefects = DefectIds;
            BindDefects();
            GC.Collect();
        }
        public void BindDefects()
        {
            panel1.Controls.Clear();
            DataTable dt = new DataTable();
            dt = objDefect.GetAllDefectsColdend(Id);
            int pointX = 30;
            int pointY = 20;
            int i = 1;
            foreach (DataRow dr in dt.Rows)
            {
                Panel mainPanel = new Panel();
                mainPanel.Name = "mainPanel" + i;
                mainPanel.Height = 290;
                mainPanel.Width = 350;
                mainPanel.Location = new Point(pointX, pointY);
                mainPanel.BorderStyle = BorderStyle.Fixed3D;
                mainPanel.AllowDrop = true;
                mainPanel.DragEnter += panel_DragEnter;
                mainPanel.DragDrop += panel_DragDrop;
                // mainPanel.BorderStyle = BorderStyle.None;
                Panel pnl = new Panel();
                pnl.Name = "pnl" + i;
                pnl.Height = 290;
                pnl.Width = 350;
                pnl.MouseDown += pnl_MouseDown;
                // pnl.Location = new Point(pointX, pointY);
                //   pnl.BorderStyle = BorderStyle.Fixed3D;
                CheckBox chk = new CheckBox();
                chk.Name = "chk" + i;
                chk.Enabled = false;
                chk.Appearance = Appearance.Button;
                chk.BackgroundImage = CommonWeb.Base64ToImage(dr["Image"].ToString());
                chk.BackgroundImageLayout = ImageLayout.Stretch;
                chk.Height = 260;
                chk.Width = 350;
                // chk.Text = dr["DefectName"].ToString();
                // chk.TextImageRelation = TextImageRelation.ImageAboveText;
                // chk.ForeColor = Color.Black;
                // chk.TextAlign = ContentAlignment.BottomCenter;
                //  chk.CheckedChanged += Chk_CheckedChanged;
                //  ControlMover.Init(pnl);
                pnl.Controls.Add(chk);
                //  ControlMover.Init(chk);
                TextBox txt = new TextBox();
                txt.Name = "txt" + i;
                txt.Text = dr["DefectName"].ToString();
                txt.ForeColor = Color.Black;
                txt.ReadOnly = true;
                txt.BorderStyle = BorderStyle.None;
                txt.Location = new Point(0, 260);
                txt.Width = 350;
                txt.Font = new Font(txt.Font.FontFamily, 12);
                txt.TextAlign = HorizontalAlignment.Center;

                pnl.Controls.Add(txt);
                // ControlMover.Init(txt);
                Label lbl = new Label();
                lbl.Name = "lbl" + i;
                lbl.Text = dr["DefectId"].ToString();
                pnl.Controls.Add(lbl);
                pointX += 400;
                if (i % 3 == 0)
                {
                    pointX = 30;
                    pointY += 320;
                }

                mainPanel.Controls.Add(pnl);
                panel1.Controls.Add(mainPanel);

                i++;
            }

            //if (parentControl.Controls.Count > 0)
            //{
            //    Controls.Find(parentControl.Name, true).First().Padding = new Padding(3, 3, 30, 3);
            //}
            //else
            //{
            //    Controls.Find(parentControl.Name, true).First().Padding = new Padding(0);
            //}
        }

        void pnl_MouseDown(object sender, MouseEventArgs e)
        {
            //((Panel)((Panel)sender).Parent).BorderStyle = BorderStyle.Fixed3D;
            // ((Panel)((Panel)sender).Parent).BackColor = Color.Red;

            ((Panel)sender).DoDragDrop(((Panel)sender), DragDropEffects.Move);
        }

        void panel_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        void panel_DragDrop(object sender, DragEventArgs e)
        {
            if (((Panel)sender).Controls[0].Parent.Parent == ((Panel)e.Data.GetData(typeof(Panel))).Parent.Parent)
            {
                Control parent = ((Panel)e.Data.GetData(typeof(Panel))).Parent;
                ((Panel)sender).Controls[0].Parent = ((Panel)e.Data.GetData(typeof(Panel))).Parent;
                ((Panel)e.Data.GetData(typeof(Panel))).Parent = (Panel)sender;

                DataTable tb = new DataTable();
                tb.Columns.Add(new DataColumn("DefectId", typeof(int)));
                tb.Columns.Add(new DataColumn("Order", typeof(int)));
                DataRow dr = tb.NewRow();
                foreach (Control ctrl in ((Panel)sender).Controls[0].Controls)
                {
                    if (ctrl.GetType() == typeof(Label))
                    {
                        dr[0] = ((Label)ctrl).Text;
                        break;
                    }
                }

                dr[1] = ((Panel)sender).Controls[0].Parent.Name.Replace("mainPanel", "");
                tb.Rows.Add(dr);

                dr = tb.NewRow();
                foreach (Control ctrl in parent.Controls[0].Controls)
                {
                    if (ctrl.GetType() == typeof(Label))
                    {
                        dr[0] = ((Label)ctrl).Text;
                        break;
                    }
                }

                dr[1] = parent.Name.Replace("mainPanel", "");
                tb.Rows.Add(dr);

                objDefect.UpdateDefectOrder(tb, Id);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DefectShowHide popup = new DefectShowHide(Id);

            popup.Show();
        }


    }
}
