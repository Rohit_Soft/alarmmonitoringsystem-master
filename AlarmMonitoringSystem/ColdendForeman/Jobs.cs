﻿using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;
using System;
using System.Windows.Forms;

namespace AlarmMonitoringSystem.ColdendForeman
{
    public partial class Jobs : Form
    {
        SetupNewJobBAL objSetupNewJob = new SetupNewJobBAL();
        public Jobs()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            dataGridView1.DataSource = objSetupNewJob.GetAllSetupJobs();
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            
               Defect obj = new Defect(Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString()), dataGridView1.Rows[e.RowIndex].Cells["LineNumber"].Value.ToString());
            obj.ShowDialog();
            //Defect.DefectIds = dataGridView1.Rows[e.RowIndex].Cells[12].Value.ToString();
        }
    }
}
