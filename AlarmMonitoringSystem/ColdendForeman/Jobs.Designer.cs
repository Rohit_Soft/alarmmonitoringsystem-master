﻿namespace AlarmMonitoringSystem.ColdendForeman
{
    partial class Jobs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.NewJobId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LineNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArticleCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductionOrderNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArticleNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArticleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Customer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Efficiency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LineSpeed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArticlePerMinute = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ApproxDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DefectIds = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DefectName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NewJobId,
            this.LineNumber,
            this.ArticleCategory,
            this.ProductionOrderNumber,
            this.ArticleNumber,
            this.ArticleName,
            this.Customer,
            this.Qty,
            this.Efficiency,
            this.LineSpeed,
            this.ArticlePerMinute,
            this.ApproxDays,
            this.DefectIds,
            this.DefectName});
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(1103, 560);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_RowHeaderMouseClick);
            // 
            // NewJobId
            // 
            this.NewJobId.DataPropertyName = "NewJobId";
            this.NewJobId.HeaderText = "NewJobId";
            this.NewJobId.Name = "NewJobId";
            this.NewJobId.ReadOnly = true;
            this.NewJobId.Visible = false;
            this.NewJobId.Width = 5;
            // 
            // LineNumber
            // 
            this.LineNumber.DataPropertyName = "LineNumber";
            this.LineNumber.HeaderText = "Line Number";
            this.LineNumber.Name = "LineNumber";
            this.LineNumber.ReadOnly = true;
            // 
            // ArticleCategory
            // 
            this.ArticleCategory.DataPropertyName = "ArticleCategory";
            this.ArticleCategory.HeaderText = "Article Category";
            this.ArticleCategory.Name = "ArticleCategory";
            this.ArticleCategory.ReadOnly = true;
            this.ArticleCategory.Visible = false;
            this.ArticleCategory.Width = 5;
            // 
            // ProductionOrderNumber
            // 
            this.ProductionOrderNumber.DataPropertyName = "ProductionOrderNumber";
            this.ProductionOrderNumber.HeaderText = "Production Order Number";
            this.ProductionOrderNumber.Name = "ProductionOrderNumber";
            this.ProductionOrderNumber.ReadOnly = true;
            this.ProductionOrderNumber.Width = 134;
            // 
            // ArticleNumber
            // 
            this.ArticleNumber.DataPropertyName = "ArticleNumber";
            this.ArticleNumber.HeaderText = "Article Number";
            this.ArticleNumber.Name = "ArticleNumber";
            this.ArticleNumber.ReadOnly = true;
            // 
            // ArticleName
            // 
            this.ArticleName.DataPropertyName = "ArticleName";
            this.ArticleName.HeaderText = "Article Name";
            this.ArticleName.Name = "ArticleName";
            this.ArticleName.ReadOnly = true;
            // 
            // Customer
            // 
            this.Customer.DataPropertyName = "Customer";
            this.Customer.HeaderText = "Customer";
            this.Customer.Name = "Customer";
            this.Customer.ReadOnly = true;
            this.Customer.Visible = false;
            this.Customer.Width = 5;
            // 
            // Qty
            // 
            this.Qty.DataPropertyName = "Qty";
            this.Qty.HeaderText = "Qty";
            this.Qty.Name = "Qty";
            this.Qty.ReadOnly = true;
            this.Qty.Visible = false;
            this.Qty.Width = 5;
            // 
            // Efficiency
            // 
            this.Efficiency.DataPropertyName = "Efficiency";
            this.Efficiency.HeaderText = "Efficiency";
            this.Efficiency.Name = "Efficiency";
            this.Efficiency.ReadOnly = true;
            this.Efficiency.Visible = false;
            this.Efficiency.Width = 5;
            // 
            // LineSpeed
            // 
            this.LineSpeed.DataPropertyName = "LineSpeed";
            this.LineSpeed.HeaderText = "Line Speed";
            this.LineSpeed.Name = "LineSpeed";
            this.LineSpeed.ReadOnly = true;
            this.LineSpeed.Visible = false;
            this.LineSpeed.Width = 116;
            // 
            // ArticlePerMinute
            // 
            this.ArticlePerMinute.DataPropertyName = "ArticlePerMinute";
            this.ArticlePerMinute.HeaderText = "Article Per Minute";
            this.ArticlePerMinute.Name = "ArticlePerMinute";
            this.ArticlePerMinute.ReadOnly = true;
            this.ArticlePerMinute.Visible = false;
            this.ArticlePerMinute.Width = 112;
            // 
            // ApproxDays
            // 
            this.ApproxDays.DataPropertyName = "ApproxDays";
            this.ApproxDays.HeaderText = "Approx Days";
            this.ApproxDays.Name = "ApproxDays";
            this.ApproxDays.ReadOnly = true;
            this.ApproxDays.Visible = false;
            this.ApproxDays.Width = 125;
            // 
            // DefectIds
            // 
            this.DefectIds.DataPropertyName = "DefectIds";
            this.DefectIds.HeaderText = "DefectIds";
            this.DefectIds.Name = "DefectIds";
            this.DefectIds.ReadOnly = true;
            this.DefectIds.Visible = false;
            // 
            // DefectName
            // 
            this.DefectName.DataPropertyName = "DefectName";
            this.DefectName.HeaderText = "Defects";
            this.DefectName.Name = "DefectName";
            this.DefectName.ReadOnly = true;
            this.DefectName.Width = 250;
            // 
            // Jobs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1132, 584);
            this.Controls.Add(this.dataGridView1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Jobs";
            this.Text = "Jobs";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn NewJobId;
        private System.Windows.Forms.DataGridViewTextBoxColumn LineNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArticleCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductionOrderNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArticleNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArticleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Customer;
        private System.Windows.Forms.DataGridViewTextBoxColumn Qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn Efficiency;
        private System.Windows.Forms.DataGridViewTextBoxColumn LineSpeed;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArticlePerMinute;
        private System.Windows.Forms.DataGridViewTextBoxColumn ApproxDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn DefectIds;
        private System.Windows.Forms.DataGridViewTextBoxColumn DefectName;
    }
}