﻿namespace AlarmMonitoringSystem
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmbUsername = new System.Windows.Forms.ComboBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.btnForgotPassword = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtFileNumber = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbLoginType = new System.Windows.Forms.ComboBox();
            this.lblLoginType = new System.Windows.Forms.Label();
            this.lblHiddenUser = new System.Windows.Forms.Label();
            this.lblHiddenLoginType = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.validator1 = new Itboy.Components.Validator(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmbUsername);
            this.panel1.Controls.Add(this.lblUsername);
            this.panel1.Controls.Add(this.btnForgotPassword);
            this.panel1.Controls.Add(this.btnLogin);
            this.panel1.Controls.Add(this.txtPassword);
            this.panel1.Controls.Add(this.txtFileNumber);
            this.panel1.Controls.Add(this.lblPassword);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cmbLoginType);
            this.panel1.Controls.Add(this.lblLoginType);
            this.panel1.Location = new System.Drawing.Point(199, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(445, 348);
            this.panel1.TabIndex = 1;
            // 
            // cmbUsername
            // 
            this.validator1.SetComparedControl(this.cmbUsername, this.lblHiddenUser);
            this.validator1.SetCompareMessage(this.cmbUsername, "User Name Required.");
            this.validator1.SetCompareOperator(this.cmbUsername, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbUsername.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUsername.FormattingEnabled = true;
            this.cmbUsername.Items.AddRange(new object[] {
            "Coldend Foreman",
            "Coldend Sorter",
            "Hot end Display",
            "Hot end Incharge",
            "CAPA Entry",
            "Report Viewer",
            "Admin"});
            this.cmbUsername.Location = new System.Drawing.Point(206, 73);
            this.cmbUsername.Name = "cmbUsername";
            this.validator1.SetRequiredMessage(this.cmbUsername, "User Name Required.");
            this.cmbUsername.Size = new System.Drawing.Size(198, 28);
            this.cmbUsername.TabIndex = 2;
            this.validator1.SetType(this.cmbUsername, ((Itboy.Components.ValidationType)((Itboy.Components.ValidationType.Required | Itboy.Components.ValidationType.Compare))));
            this.cmbUsername.SelectedIndexChanged += new System.EventHandler(this.cmbUsername_SelectedIndexChanged);
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(74, 81);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(89, 20);
            this.lblUsername.TabIndex = 9;
            this.lblUsername.Text = "User Name";
            // 
            // btnForgotPassword
            // 
            this.btnForgotPassword.CausesValidation = false;
            this.btnForgotPassword.Location = new System.Drawing.Point(256, 251);
            this.btnForgotPassword.Name = "btnForgotPassword";
            this.btnForgotPassword.Size = new System.Drawing.Size(148, 33);
            this.btnForgotPassword.TabIndex = 6;
            this.btnForgotPassword.Text = "Forgot Password";
            this.btnForgotPassword.UseVisualStyleBackColor = true;
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(122, 251);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(107, 33);
            this.btnLogin.TabIndex = 5;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(206, 166);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.validator1.SetRequiredMessage(this.txtPassword, "Password Required.");
            this.txtPassword.Size = new System.Drawing.Size(198, 26);
            this.txtPassword.TabIndex = 4;
            this.validator1.SetType(this.txtPassword, Itboy.Components.ValidationType.Required);
            // 
            // txtFileNumber
            // 
            this.txtFileNumber.Location = new System.Drawing.Point(206, 121);
            this.txtFileNumber.Name = "txtFileNumber";
            this.txtFileNumber.ReadOnly = true;
            this.validator1.SetRequiredMessage(this.txtFileNumber, "File Number Required.");
            this.txtFileNumber.Size = new System.Drawing.Size(198, 26);
            this.txtFileNumber.TabIndex = 3;
            this.txtFileNumber.TabStop = false;
            this.validator1.SetType(this.txtFileNumber, Itboy.Components.ValidationType.Required);
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(85, 172);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(78, 20);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(74, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "File Number";
            // 
            // cmbLoginType
            // 
            this.validator1.SetComparedControl(this.cmbLoginType, this.lblHiddenLoginType);
            this.validator1.SetCompareMessage(this.cmbLoginType, "Login Type Required.");
            this.validator1.SetCompareOperator(this.cmbLoginType, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbLoginType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoginType.FormattingEnabled = true;
            this.cmbLoginType.Items.AddRange(new object[] {
            "Coldend Foreman",
            "Coldend Sorter",
            "Hot end Display",
            "Hot end Incharge",
            "CAPA Entry",
            "Report Viewer",
            "Admin"});
            this.cmbLoginType.Location = new System.Drawing.Point(206, 26);
            this.cmbLoginType.Name = "cmbLoginType";
            this.validator1.SetRequiredMessage(this.cmbLoginType, "Login Type Required.");
            this.cmbLoginType.Size = new System.Drawing.Size(198, 28);
            this.cmbLoginType.TabIndex = 1;
            this.validator1.SetType(this.cmbLoginType, ((Itboy.Components.ValidationType)((Itboy.Components.ValidationType.Required | Itboy.Components.ValidationType.Compare))));
            this.cmbLoginType.SelectedIndexChanged += new System.EventHandler(this.cmbLoginType_SelectedIndexChanged);
            // 
            // lblLoginType
            // 
            this.lblLoginType.AutoSize = true;
            this.lblLoginType.Location = new System.Drawing.Point(74, 34);
            this.lblLoginType.Name = "lblLoginType";
            this.lblLoginType.Size = new System.Drawing.Size(86, 20);
            this.lblLoginType.TabIndex = 0;
            this.lblLoginType.Text = "Login Type";
            // 
            // lblHiddenUser
            // 
            this.lblHiddenUser.AutoSize = true;
            this.lblHiddenUser.Location = new System.Drawing.Point(38, 54);
            this.lblHiddenUser.Name = "lblHiddenUser";
            this.lblHiddenUser.Size = new System.Drawing.Size(92, 20);
            this.lblHiddenUser.TabIndex = 17;
            this.lblHiddenUser.Text = "Select User";
            this.lblHiddenUser.Visible = false;
            // 
            // lblHiddenLoginType
            // 
            this.lblHiddenLoginType.AutoSize = true;
            this.lblHiddenLoginType.Location = new System.Drawing.Point(21, 25);
            this.lblHiddenLoginType.Name = "lblHiddenLoginType";
            this.lblHiddenLoginType.Size = new System.Drawing.Size(135, 20);
            this.lblHiddenLoginType.TabIndex = 16;
            this.lblHiddenLoginType.Text = "Select Login Type";
            this.lblHiddenLoginType.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(25, 95);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(156, 174);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // validator1
            // 
            this.validator1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.validator1.Form = this;
            // 
            // Login
            // 
            this.AcceptButton = this.btnLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 390);
            this.Controls.Add(this.lblHiddenUser);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblHiddenLoginType);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Login";
            this.Text = "Welcome to ALARM System";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbLoginType;
        private System.Windows.Forms.Label lblLoginType;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtFileNumber;
        private System.Windows.Forms.Button btnForgotPassword;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.ComboBox cmbUsername;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblHiddenLoginType;
        private Itboy.Components.Validator validator1;
        private System.Windows.Forms.Label lblHiddenUser;
    }
}

