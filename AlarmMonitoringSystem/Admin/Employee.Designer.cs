﻿namespace AlarmMonitoringSystem.Admin
{
    partial class Employee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.SrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nationality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Department = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Section = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Employeecode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Phone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Designation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Manager = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ManagerEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoginType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtConfirmPassword = new System.Windows.Forms.TextBox();
            this.lblConfirmPassword = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblHiddenLogintype = new System.Windows.Forms.Label();
            this.cmbLoginType = new System.Windows.Forms.ComboBox();
            this.txtManagerEmail = new System.Windows.Forms.TextBox();
            this.lblLoginType = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbManager = new System.Windows.Forms.ComboBox();
            this.lblManager = new System.Windows.Forms.Label();
            this.lblHiddenDesignation = new System.Windows.Forms.Label();
            this.cmbDesignation = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPhoneNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.lblLastname = new System.Windows.Forms.Label();
            this.txtEmpId = new System.Windows.Forms.TextBox();
            this.lblEmpId = new System.Windows.Forms.Label();
            this.lblHiddenNationality = new System.Windows.Forms.Label();
            this.lblHiddenSection = new System.Windows.Forms.Label();
            this.lblHiddenDept = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.txtFileNumber = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.cmbDepartment = new System.Windows.Forms.ComboBox();
            this.cmbSection = new System.Windows.Forms.ComboBox();
            this.cmbNationality = new System.Windows.Forms.ComboBox();
            this.lblFileNumber = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblDepartment = new System.Windows.Forms.Label();
            this.lblSection = new System.Windows.Forms.Label();
            this.lblNationality = new System.Windows.Forms.Label();
            this.validator1 = new Itboy.Components.Validator(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Location = new System.Drawing.Point(12, 408);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1463, 473);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Employees";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SrNo,
            this.Nationality,
            this.Department,
            this.Section,
            this.FirstName,
            this.LastName,
            this.FileNumber,
            this.Employeecode,
            this.Email,
            this.Phone,
            this.Designation,
            this.Manager,
            this.ManagerEmail,
            this.LoginType,
            this.Username,
            this.EmployeeId});
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(1450, 347);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_RowHeaderMouseClick_1);
            // 
            // SrNo
            // 
            this.SrNo.DataPropertyName = "SrNo";
            this.SrNo.HeaderText = "Sr.No.";
            this.SrNo.Name = "SrNo";
            this.SrNo.ReadOnly = true;
            this.SrNo.Width = 60;
            // 
            // Nationality
            // 
            this.Nationality.DataPropertyName = "Nationality";
            this.Nationality.HeaderText = "Nationality";
            this.Nationality.Name = "Nationality";
            this.Nationality.ReadOnly = true;
            this.Nationality.Visible = false;
            this.Nationality.Width = 5;
            // 
            // Department
            // 
            this.Department.DataPropertyName = "Department";
            this.Department.HeaderText = "Department";
            this.Department.Name = "Department";
            this.Department.ReadOnly = true;
            this.Department.Visible = false;
            this.Department.Width = 5;
            // 
            // Section
            // 
            this.Section.DataPropertyName = "Section";
            this.Section.HeaderText = "Section";
            this.Section.Name = "Section";
            this.Section.ReadOnly = true;
            this.Section.Visible = false;
            this.Section.Width = 5;
            // 
            // FirstName
            // 
            this.FirstName.DataPropertyName = "FirstName";
            this.FirstName.HeaderText = "First Name";
            this.FirstName.Name = "FirstName";
            this.FirstName.ReadOnly = true;
            // 
            // LastName
            // 
            this.LastName.DataPropertyName = "LastName";
            this.LastName.HeaderText = "Last Name";
            this.LastName.Name = "LastName";
            this.LastName.ReadOnly = true;
            // 
            // FileNumber
            // 
            this.FileNumber.DataPropertyName = "FileNumber";
            this.FileNumber.HeaderText = "File Number";
            this.FileNumber.Name = "FileNumber";
            this.FileNumber.ReadOnly = true;
            this.FileNumber.Width = 130;
            // 
            // Employeecode
            // 
            this.Employeecode.DataPropertyName = "Employeecode";
            this.Employeecode.HeaderText = "Employee ID";
            this.Employeecode.Name = "Employeecode";
            this.Employeecode.ReadOnly = true;
            // 
            // Email
            // 
            this.Email.DataPropertyName = "email";
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            this.Email.Width = 130;
            // 
            // Phone
            // 
            this.Phone.DataPropertyName = "Phone";
            this.Phone.HeaderText = "Phone";
            this.Phone.Name = "Phone";
            this.Phone.ReadOnly = true;
            // 
            // Designation
            // 
            this.Designation.DataPropertyName = "Designation";
            this.Designation.HeaderText = "Designation";
            this.Designation.Name = "Designation";
            this.Designation.ReadOnly = true;
            // 
            // Manager
            // 
            this.Manager.DataPropertyName = "Manager";
            this.Manager.HeaderText = "Manager";
            this.Manager.Name = "Manager";
            this.Manager.ReadOnly = true;
            // 
            // ManagerEmail
            // 
            this.ManagerEmail.DataPropertyName = "ManagerEmail";
            this.ManagerEmail.HeaderText = "Manager Email";
            this.ManagerEmail.Name = "ManagerEmail";
            this.ManagerEmail.ReadOnly = true;
            this.ManagerEmail.Visible = false;
            this.ManagerEmail.Width = 5;
            // 
            // LoginType
            // 
            this.LoginType.DataPropertyName = "LoginType";
            this.LoginType.HeaderText = "Login Type";
            this.LoginType.Name = "LoginType";
            this.LoginType.ReadOnly = true;
            this.LoginType.Visible = false;
            this.LoginType.Width = 5;
            // 
            // Username
            // 
            this.Username.DataPropertyName = "Username";
            this.Username.HeaderText = "User Name";
            this.Username.Name = "Username";
            this.Username.ReadOnly = true;
            this.Username.Visible = false;
            this.Username.Width = 5;
            // 
            // EmployeeId
            // 
            this.EmployeeId.DataPropertyName = "EmployeeId";
            this.EmployeeId.HeaderText = "EmployeeId";
            this.EmployeeId.Name = "EmployeeId";
            this.EmployeeId.Visible = false;
            this.EmployeeId.Width = 10;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtConfirmPassword);
            this.panel1.Controls.Add(this.lblConfirmPassword);
            this.panel1.Controls.Add(this.txtPassword);
            this.panel1.Controls.Add(this.lblPassword);
            this.panel1.Controls.Add(this.txtUsername);
            this.panel1.Controls.Add(this.lblUsername);
            this.panel1.Controls.Add(this.lblHiddenLogintype);
            this.panel1.Controls.Add(this.cmbLoginType);
            this.panel1.Controls.Add(this.txtManagerEmail);
            this.panel1.Controls.Add(this.lblLoginType);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cmbManager);
            this.panel1.Controls.Add(this.lblManager);
            this.panel1.Controls.Add(this.lblHiddenDesignation);
            this.panel1.Controls.Add(this.cmbDesignation);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtPhoneNumber);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtEmail);
            this.panel1.Controls.Add(this.lblEmail);
            this.panel1.Controls.Add(this.txtLastName);
            this.panel1.Controls.Add(this.lblLastname);
            this.panel1.Controls.Add(this.txtEmpId);
            this.panel1.Controls.Add(this.lblEmpId);
            this.panel1.Controls.Add(this.lblHiddenNationality);
            this.panel1.Controls.Add(this.lblHiddenSection);
            this.panel1.Controls.Add(this.lblHiddenDept);
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnSubmit);
            this.panel1.Controls.Add(this.txtFileNumber);
            this.panel1.Controls.Add(this.txtFirstName);
            this.panel1.Controls.Add(this.cmbDepartment);
            this.panel1.Controls.Add(this.cmbSection);
            this.panel1.Controls.Add(this.cmbNationality);
            this.panel1.Controls.Add(this.lblFileNumber);
            this.panel1.Controls.Add(this.lblName);
            this.panel1.Controls.Add(this.lblDepartment);
            this.panel1.Controls.Add(this.lblSection);
            this.panel1.Controls.Add(this.lblNationality);
            this.panel1.Location = new System.Drawing.Point(12, 11);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1463, 391);
            this.panel1.TabIndex = 4;
            // 
            // txtConfirmPassword
            // 
            this.validator1.SetComparedControl(this.txtConfirmPassword, this.txtPassword);
            this.validator1.SetCompareMessage(this.txtConfirmPassword, "Password and Confirm Password must be same.");
            this.validator1.SetCompareOperator(this.txtConfirmPassword, Itboy.Components.ValidationCompareOperator.Equal);
            this.txtConfirmPassword.Location = new System.Drawing.Point(186, 279);
            this.txtConfirmPassword.MaxLength = 50;
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.PasswordChar = '*';
            this.validator1.SetRequiredMessage(this.txtConfirmPassword, "Confirm Password Required.");
            this.txtConfirmPassword.Size = new System.Drawing.Size(282, 26);
            this.txtConfirmPassword.TabIndex = 16;
            this.validator1.SetType(this.txtConfirmPassword, ((Itboy.Components.ValidationType)((Itboy.Components.ValidationType.Required | Itboy.Components.ValidationType.Compare))));
            // 
            // lblConfirmPassword
            // 
            this.lblConfirmPassword.AutoSize = true;
            this.lblConfirmPassword.Location = new System.Drawing.Point(5, 282);
            this.lblConfirmPassword.Name = "lblConfirmPassword";
            this.lblConfirmPassword.Size = new System.Drawing.Size(137, 20);
            this.lblConfirmPassword.TabIndex = 35;
            this.lblConfirmPassword.Text = "Confirm Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(1148, 228);
            this.txtPassword.MaxLength = 50;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.validator1.SetRequiredMessage(this.txtPassword, "Password Required.");
            this.txtPassword.Size = new System.Drawing.Size(282, 26);
            this.txtPassword.TabIndex = 15;
            this.validator1.SetType(this.txtPassword, Itboy.Components.ValidationType.Required);
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(1021, 229);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(78, 20);
            this.lblPassword.TabIndex = 32;
            this.lblPassword.Text = "Password";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(668, 226);
            this.txtUsername.MaxLength = 50;
            this.txtUsername.Name = "txtUsername";
            this.validator1.SetRequiredMessage(this.txtUsername, "User Name Required.");
            this.txtUsername.Size = new System.Drawing.Size(282, 26);
            this.txtUsername.TabIndex = 14;
            this.validator1.SetType(this.txtUsername, Itboy.Components.ValidationType.Required);
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(535, 229);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(89, 20);
            this.lblUsername.TabIndex = 31;
            this.lblUsername.Text = "User Name";
            // 
            // lblHiddenLogintype
            // 
            this.lblHiddenLogintype.AutoSize = true;
            this.lblHiddenLogintype.Location = new System.Drawing.Point(601, 337);
            this.lblHiddenLogintype.Name = "lblHiddenLogintype";
            this.lblHiddenLogintype.Size = new System.Drawing.Size(135, 20);
            this.lblHiddenLogintype.TabIndex = 29;
            this.lblHiddenLogintype.Text = "Select Login Type";
            this.lblHiddenLogintype.Visible = false;
            // 
            // cmbLoginType
            // 
            this.validator1.SetComparedControl(this.cmbLoginType, this.lblHiddenLogintype);
            this.validator1.SetCompareMessage(this.cmbLoginType, "Login Type Required.");
            this.validator1.SetCompareOperator(this.cmbLoginType, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbLoginType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoginType.FormattingEnabled = true;
            this.cmbLoginType.Location = new System.Drawing.Point(186, 226);
            this.cmbLoginType.Name = "cmbLoginType";
            this.cmbLoginType.Size = new System.Drawing.Size(282, 28);
            this.cmbLoginType.TabIndex = 13;
            this.validator1.SetType(this.cmbLoginType, Itboy.Components.ValidationType.Compare);
            // 
            // txtManagerEmail
            // 
            this.txtManagerEmail.Location = new System.Drawing.Point(1148, 177);
            this.txtManagerEmail.MaxLength = 50;
            this.txtManagerEmail.Name = "txtManagerEmail";
            this.txtManagerEmail.ReadOnly = true;
            this.txtManagerEmail.Size = new System.Drawing.Size(282, 26);
            this.txtManagerEmail.TabIndex = 12;
            // 
            // lblLoginType
            // 
            this.lblLoginType.AutoSize = true;
            this.lblLoginType.Location = new System.Drawing.Point(56, 229);
            this.lblLoginType.Name = "lblLoginType";
            this.lblLoginType.Size = new System.Drawing.Size(86, 20);
            this.lblLoginType.TabIndex = 3;
            this.lblLoginType.Text = "Login Type";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(984, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 20);
            this.label3.TabIndex = 28;
            this.label3.Text = "Manager Email";
            // 
            // cmbManager
            // 
            this.cmbManager.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbManager.FormattingEnabled = true;
            this.cmbManager.Location = new System.Drawing.Point(668, 175);
            this.cmbManager.Name = "cmbManager";
            this.cmbManager.Size = new System.Drawing.Size(282, 28);
            this.cmbManager.TabIndex = 11;
            this.cmbManager.SelectedIndexChanged += new System.EventHandler(this.cmbManager_SelectedIndexChanged);
            // 
            // lblManager
            // 
            this.lblManager.AutoSize = true;
            this.lblManager.Location = new System.Drawing.Point(552, 178);
            this.lblManager.Name = "lblManager";
            this.lblManager.Size = new System.Drawing.Size(72, 20);
            this.lblManager.TabIndex = 26;
            this.lblManager.Text = "Manager";
            // 
            // lblHiddenDesignation
            // 
            this.lblHiddenDesignation.AutoSize = true;
            this.lblHiddenDesignation.Location = new System.Drawing.Point(434, 333);
            this.lblHiddenDesignation.Name = "lblHiddenDesignation";
            this.lblHiddenDesignation.Size = new System.Drawing.Size(143, 20);
            this.lblHiddenDesignation.TabIndex = 25;
            this.lblHiddenDesignation.Text = "Select Designation";
            this.lblHiddenDesignation.Visible = false;
            // 
            // cmbDesignation
            // 
            this.validator1.SetComparedControl(this.cmbDesignation, this.lblHiddenDesignation);
            this.validator1.SetCompareMessage(this.cmbDesignation, "Designation Required.");
            this.validator1.SetCompareOperator(this.cmbDesignation, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbDesignation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDesignation.FormattingEnabled = true;
            this.cmbDesignation.Location = new System.Drawing.Point(186, 174);
            this.cmbDesignation.Name = "cmbDesignation";
            this.cmbDesignation.Size = new System.Drawing.Size(282, 28);
            this.cmbDesignation.TabIndex = 10;
            this.validator1.SetType(this.cmbDesignation, Itboy.Components.ValidationType.Compare);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 178);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 20);
            this.label2.TabIndex = 24;
            this.label2.Text = "Designation";
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.Location = new System.Drawing.Point(1148, 123);
            this.txtPhoneNumber.MaxLength = 20;
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.validator1.SetRangeOfLowerBound(this.txtPhoneNumber, "0");
            this.validator1.SetRangeOfUpperBound(this.txtPhoneNumber, "999999");
            this.validator1.SetRequiredMessage(this.txtPhoneNumber, "Phone Number Required.");
            this.txtPhoneNumber.Size = new System.Drawing.Size(282, 26);
            this.txtPhoneNumber.TabIndex = 9;
            this.validator1.SetType(this.txtPhoneNumber, Itboy.Components.ValidationType.Required);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(984, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 20);
            this.label1.TabIndex = 22;
            this.label1.Text = "Phone Number";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(668, 123);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.validator1.SetRegularExpression(this.txtEmail, "/^(([^<>()\\[\\]\\.,;:\\s@\\\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@(([^<>()[\\]\\.,;:" +
        "\\s@\\\"]+\\.)+[^<>()[\\]\\.,;:\\s@\\\"]{2,})$/i");
            this.validator1.SetRegularExpressionMessage(this.txtEmail, "Valid Email Required.");
            this.validator1.SetRequiredMessage(this.txtEmail, "Email Required.");
            this.txtEmail.Size = new System.Drawing.Size(282, 26);
            this.txtEmail.TabIndex = 8;
            this.validator1.SetType(this.txtEmail, Itboy.Components.ValidationType.Required);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(576, 129);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(48, 20);
            this.lblEmail.TabIndex = 8;
            this.lblEmail.Text = "Email";
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(668, 72);
            this.txtLastName.MaxLength = 50;
            this.txtLastName.Name = "txtLastName";
            this.validator1.SetRequiredMessage(this.txtLastName, "Last Name Required.");
            this.txtLastName.Size = new System.Drawing.Size(282, 26);
            this.txtLastName.TabIndex = 5;
            this.validator1.SetType(this.txtLastName, Itboy.Components.ValidationType.Required);
            // 
            // lblLastname
            // 
            this.lblLastname.AutoSize = true;
            this.lblLastname.Location = new System.Drawing.Point(538, 75);
            this.lblLastname.Name = "lblLastname";
            this.lblLastname.Size = new System.Drawing.Size(86, 20);
            this.lblLastname.TabIndex = 19;
            this.lblLastname.Text = "Last Name";
            // 
            // txtEmpId
            // 
            this.validator1.SetCompareMessage(this.txtEmpId, "Employee ID must be 4 digit.");
            this.validator1.SetDataType(this.txtEmpId, Itboy.Components.ValidationDataType.Integer);
            this.txtEmpId.Location = new System.Drawing.Point(186, 123);
            this.txtEmpId.MaxLength = 4;
            this.txtEmpId.Name = "txtEmpId";
            this.validator1.SetRangeMessage(this.txtEmpId, "Employee ID must be 4 digit.");
            this.validator1.SetRangeOfLowerBound(this.txtEmpId, "1000");
            this.validator1.SetRangeOfUpperBound(this.txtEmpId, "9999");
            this.validator1.SetRequiredMessage(this.txtEmpId, "Employee ID Required.");
            this.txtEmpId.Size = new System.Drawing.Size(282, 26);
            this.txtEmpId.TabIndex = 7;
            this.validator1.SetType(this.txtEmpId, ((Itboy.Components.ValidationType)(((Itboy.Components.ValidationType.Required | Itboy.Components.ValidationType.Compare) 
                | Itboy.Components.ValidationType.Range))));
            // 
            // lblEmpId
            // 
            this.lblEmpId.AutoSize = true;
            this.lblEmpId.Location = new System.Drawing.Point(42, 126);
            this.lblEmpId.Name = "lblEmpId";
            this.lblEmpId.Size = new System.Drawing.Size(100, 20);
            this.lblEmpId.TabIndex = 18;
            this.lblEmpId.Text = "Employee ID";
            // 
            // lblHiddenNationality
            // 
            this.lblHiddenNationality.AutoSize = true;
            this.lblHiddenNationality.Location = new System.Drawing.Point(283, 333);
            this.lblHiddenNationality.Name = "lblHiddenNationality";
            this.lblHiddenNationality.Size = new System.Drawing.Size(131, 20);
            this.lblHiddenNationality.TabIndex = 17;
            this.lblHiddenNationality.Text = "Select Nationality";
            this.lblHiddenNationality.Visible = false;
            // 
            // lblHiddenSection
            // 
            this.lblHiddenSection.AutoSize = true;
            this.lblHiddenSection.Location = new System.Drawing.Point(154, 333);
            this.lblHiddenSection.Name = "lblHiddenSection";
            this.lblHiddenSection.Size = new System.Drawing.Size(112, 20);
            this.lblHiddenSection.TabIndex = 16;
            this.lblHiddenSection.Text = "Select Section";
            this.lblHiddenSection.Visible = false;
            // 
            // lblHiddenDept
            // 
            this.lblHiddenDept.AutoSize = true;
            this.lblHiddenDept.Location = new System.Drawing.Point(5, 333);
            this.lblHiddenDept.Name = "lblHiddenDept";
            this.lblHiddenDept.Size = new System.Drawing.Size(143, 20);
            this.lblHiddenDept.TabIndex = 15;
            this.lblHiddenDept.Text = "Select Department";
            this.lblHiddenDept.Visible = false;
            // 
            // btnClear
            // 
            this.btnClear.CausesValidation = false;
            this.btnClear.Location = new System.Drawing.Point(1322, 333);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(108, 33);
            this.btnClear.TabIndex = 19;
            this.btnClear.Text = "Add";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(1181, 333);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(108, 33);
            this.btnDelete.TabIndex = 18;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(1038, 333);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(108, 33);
            this.btnSubmit.TabIndex = 17;
            this.btnSubmit.Text = "Save";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click_1);
            // 
            // txtFileNumber
            // 
            this.txtFileNumber.Location = new System.Drawing.Point(1148, 71);
            this.txtFileNumber.MaxLength = 5;
            this.txtFileNumber.Name = "txtFileNumber";
            this.validator1.SetRequiredMessage(this.txtFileNumber, "File Number Required.");
            this.txtFileNumber.Size = new System.Drawing.Size(282, 26);
            this.txtFileNumber.TabIndex = 6;
            this.validator1.SetType(this.txtFileNumber, Itboy.Components.ValidationType.Required);
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(186, 72);
            this.txtFirstName.MaxLength = 50;
            this.txtFirstName.Name = "txtFirstName";
            this.validator1.SetRequiredMessage(this.txtFirstName, "First Name Required.");
            this.txtFirstName.Size = new System.Drawing.Size(282, 26);
            this.txtFirstName.TabIndex = 4;
            this.validator1.SetType(this.txtFirstName, Itboy.Components.ValidationType.Required);
            // 
            // cmbDepartment
            // 
            this.validator1.SetComparedControl(this.cmbDepartment, this.lblHiddenDept);
            this.validator1.SetCompareMessage(this.cmbDepartment, "Department Required.");
            this.validator1.SetCompareOperator(this.cmbDepartment, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(668, 17);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(282, 28);
            this.cmbDepartment.TabIndex = 2;
            this.validator1.SetType(this.cmbDepartment, Itboy.Components.ValidationType.Compare);
            this.cmbDepartment.SelectedIndexChanged += new System.EventHandler(this.cmbDepartment_SelectedIndexChanged);
            // 
            // cmbSection
            // 
            this.validator1.SetComparedControl(this.cmbSection, this.lblHiddenSection);
            this.validator1.SetCompareMessage(this.cmbSection, "Section Required.");
            this.validator1.SetCompareOperator(this.cmbSection, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSection.FormattingEnabled = true;
            this.cmbSection.Location = new System.Drawing.Point(1148, 17);
            this.cmbSection.Name = "cmbSection";
            this.cmbSection.Size = new System.Drawing.Size(282, 28);
            this.cmbSection.TabIndex = 3;
            this.validator1.SetType(this.cmbSection, Itboy.Components.ValidationType.Compare);
            // 
            // cmbNationality
            // 
            this.validator1.SetComparedControl(this.cmbNationality, this.lblHiddenNationality);
            this.validator1.SetCompareMessage(this.cmbNationality, "Nationality Required.");
            this.validator1.SetCompareOperator(this.cmbNationality, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbNationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNationality.FormattingEnabled = true;
            this.cmbNationality.Location = new System.Drawing.Point(186, 17);
            this.cmbNationality.Name = "cmbNationality";
            this.cmbNationality.Size = new System.Drawing.Size(282, 28);
            this.cmbNationality.TabIndex = 1;
            this.validator1.SetType(this.cmbNationality, Itboy.Components.ValidationType.Compare);
            // 
            // lblFileNumber
            // 
            this.lblFileNumber.AutoSize = true;
            this.lblFileNumber.Location = new System.Drawing.Point(1005, 75);
            this.lblFileNumber.Name = "lblFileNumber";
            this.lblFileNumber.Size = new System.Drawing.Size(94, 20);
            this.lblFileNumber.TabIndex = 4;
            this.lblFileNumber.Text = "File Number";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(56, 75);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(86, 20);
            this.lblName.TabIndex = 3;
            this.lblName.Text = "First Name";
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            this.lblDepartment.Location = new System.Drawing.Point(530, 20);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(94, 20);
            this.lblDepartment.TabIndex = 2;
            this.lblDepartment.Text = "Department";
            // 
            // lblSection
            // 
            this.lblSection.AutoSize = true;
            this.lblSection.Location = new System.Drawing.Point(1036, 20);
            this.lblSection.Name = "lblSection";
            this.lblSection.Size = new System.Drawing.Size(63, 20);
            this.lblSection.TabIndex = 1;
            this.lblSection.Text = "Section";
            // 
            // lblNationality
            // 
            this.lblNationality.AutoSize = true;
            this.lblNationality.Location = new System.Drawing.Point(60, 20);
            this.lblNationality.Name = "lblNationality";
            this.lblNationality.Size = new System.Drawing.Size(82, 20);
            this.lblNationality.TabIndex = 0;
            this.lblNationality.Text = "Nationality";
            // 
            // validator1
            // 
            this.validator1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            // 
            // Employee
            // 
            this.AcceptButton = this.btnSubmit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1487, 621);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Employee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.Label lblConfirmPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblHiddenLogintype;
        private System.Windows.Forms.ComboBox cmbLoginType;
        private System.Windows.Forms.TextBox txtManagerEmail;
        private Itboy.Components.Validator validator1;
        private System.Windows.Forms.Label lblLoginType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbManager;
        private System.Windows.Forms.Label lblManager;
        private System.Windows.Forms.Label lblHiddenDesignation;
        private System.Windows.Forms.ComboBox cmbDesignation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPhoneNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label lblLastname;
        private System.Windows.Forms.TextBox txtEmpId;
        private System.Windows.Forms.Label lblEmpId;
        private System.Windows.Forms.Label lblHiddenNationality;
        private System.Windows.Forms.Label lblHiddenSection;
        private System.Windows.Forms.Label lblHiddenDept;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.TextBox txtFileNumber;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.ComboBox cmbDepartment;
        private System.Windows.Forms.ComboBox cmbSection;
        private System.Windows.Forms.ComboBox cmbNationality;
        private System.Windows.Forms.Label lblFileNumber;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblDepartment;
        private System.Windows.Forms.Label lblSection;
        private System.Windows.Forms.Label lblNationality;
        private System.Windows.Forms.DataGridViewTextBoxColumn SrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nationality;
        private System.Windows.Forms.DataGridViewTextBoxColumn Department;
        private System.Windows.Forms.DataGridViewTextBoxColumn Section;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Employeecode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Phone;
        private System.Windows.Forms.DataGridViewTextBoxColumn Designation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Manager;
        private System.Windows.Forms.DataGridViewTextBoxColumn ManagerEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoginType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Username;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeId;
    }
}