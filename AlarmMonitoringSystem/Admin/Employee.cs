﻿using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;
using System;
using System.Windows.Forms;

namespace AlarmMonitoringSystem.Admin
{
    public partial class Employee : Form
    {
        EmployeeBAL objEmp = new EmployeeBAL();
        int id = 0;
        public Employee()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            BindEmployees();
            BindNationality();
            BindDepartments();
            BindDesignation();
            BindManager();
            BindLoginType();
        }

        private void BindEmployees()
        {
            dataGridView1.DataSource = objEmp.GetAllDepartments(false);
        }

        private void BindNationality()
        {
            cmbNationality.ValueMember = "NationalityId";
            cmbNationality.DisplayMember = "Nationality";
            cmbNationality.DataSource = objEmp.GetAllNationality();
        }

        private void BindDesignation()
        {
            cmbDesignation.ValueMember = "DesignationId";
            cmbDesignation.DisplayMember = "Designation";
            cmbDesignation.DataSource = objEmp.GetAllDesignation();
        }

        private void BindManager()
        {
            cmbManager.ValueMember = "EmployeeId";
            cmbManager.DisplayMember = "Name";
            cmbManager.DataSource = objEmp.GetAllManagers(id);
        }
        private void BindLoginType()
        {
            RegistrationBAL obj = new RegistrationBAL();
            cmbLoginType.ValueMember = "LoginTypeId";
            cmbLoginType.DisplayMember = "LoginType";
            cmbLoginType.DataSource = obj.GetLoginTypes();
        }

        private void BindDepartments()
        {
            DepartmentBAL obj = new DepartmentBAL();
            cmbDepartment.ValueMember = "DepartmentId";
            cmbDepartment.DisplayMember = "Department";
            cmbDepartment.DataSource = obj.GetAllDepartments(true);
        }

        private void BindSection()
        {
            SectionBAL obj = new SectionBAL();
            cmbSection.ValueMember = "SectionId";
            cmbSection.DisplayMember = "Section";
            cmbSection.DataSource = obj.GetAllSectionsByDepartment(Convert.ToInt32(cmbDepartment.SelectedValue.ToString()));
        }

        private void Clear()
        {
            CausesValidation = false;
            id = 0;
            txtFileNumber.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            cmbDepartment.SelectedIndex = 0;
            cmbNationality.SelectedIndex = 0;
            cmbSection.SelectedIndex = 0;
            txtConfirmPassword.Text = string.Empty;
            txtEmail.Text= string.Empty;
            txtEmpId.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtManagerEmail.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtPhoneNumber.Text = string.Empty;
            txtUsername.Text = string.Empty;
            txtPassword.Enabled = true;
            txtConfirmPassword.Enabled = true;
            txtPassword.Visible = txtConfirmPassword.Visible = lblPassword.Visible = lblConfirmPassword.Visible = true;
            validator1.Dispose();
            BindEmployees();
            BindManager();
        }


        private void btnSubmit_Click_1(object sender, EventArgs e)
        {
            if (validator1.Validate())
            {
                objEmp.EmployeeId = id;
                objEmp.DesignationId = Convert.ToInt32(cmbDesignation.SelectedValue.ToString());
                objEmp.FileNumber = txtFileNumber.Text;
                objEmp.NationalityId = Convert.ToInt32(cmbNationality.SelectedValue.ToString());
                objEmp.DepartmentId = Convert.ToInt32(cmbDepartment.SelectedValue.ToString());
                objEmp.SectionId = Convert.ToInt32(cmbSection.SelectedValue.ToString());
                objEmp.CreatedBy = UserDetail.userId;
                objEmp.Email = txtEmail.Text;
                objEmp.EmployeeCode = txtEmpId.Text;
                objEmp.FirstName = txtFirstName.Text;
                objEmp.LastName = txtLastName.Text;
                objEmp.LoginTypeId = Convert.ToInt32(cmbLoginType.SelectedValue.ToString());
                objEmp.ManagerId = Convert.ToInt32(cmbManager.SelectedValue.ToString());
                objEmp.Password = Encrypt.EncryptString(txtPassword.Text);
                objEmp.Phone = txtPhoneNumber.Text;
                objEmp.UserName = txtUsername.Text;
                int retValue = objEmp.InsertUpdateEmployee(objEmp);

                if (retValue == -1)
                {
                    CommonWeb.Alert("File Number already exist.", "error");
                }
                else if (retValue == -2)
                {
                    CommonWeb.Alert("Employee ID already exist.", "error");
                }
                else if (retValue == -2)
                {
                    CommonWeb.Alert("User Name already exist.", "error");
                }
                else if (retValue > 0)
                {
                    CommonWeb.Alert("Employee " + (id == 0 ? "inserted" : "updated") + " successfully.", "success");
                    Clear();
                }
                else
                {
                    CommonWeb.Alert("Problem in " + (id == 0 ? "inserting" : "updating") + " Employee. Please contact administrator.", "error");
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void cmbDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSection();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (id == 0)
            {
                CommonWeb.Alert("Please select an Employee to delete", "error");
            }
            else {
                objEmp.EmployeeId = id;
                objEmp.CreatedBy = UserDetail.userId;
                int retValue = objEmp.DeleteEmployee(objEmp);
                if (retValue > 0)
                {
                    CommonWeb.Alert("Employee deleted successfully.", "success");
                    Clear();
                }
                else
                {
                    CommonWeb.Alert("Problem in deleting Employee. Please contact administrator.", "error");
                }
            }
        }

        private void dataGridView1_RowHeaderMouseClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
            id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString());
            BindManager();
            txtFileNumber.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtLastName.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtFirstName.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            cmbNationality.SelectedIndex = cmbNationality.FindStringExact(dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString());
            cmbDepartment.SelectedIndex = cmbDepartment.FindStringExact(dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString());
            cmbSection.SelectedIndex = cmbSection.FindStringExact(dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString());

            txtEmpId.Text = dataGridView1.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtEmail.Text = dataGridView1.Rows[e.RowIndex].Cells[9].Value.ToString();
            txtPhoneNumber.Text = dataGridView1.Rows[e.RowIndex].Cells[10].Value.ToString();
            cmbDesignation.SelectedIndex = cmbDesignation.FindStringExact(dataGridView1.Rows[e.RowIndex].Cells[11].Value.ToString());
            var manager = Convert.ToString(dataGridView1.Rows[e.RowIndex].Cells[12].Value);
            cmbManager.SelectedIndex = (manager == "" ? 0 : cmbManager.FindStringExact(manager));
            cmbLoginType.SelectedIndex = cmbLoginType.FindStringExact(dataGridView1.Rows[e.RowIndex].Cells[13].Value.ToString());
            txtUsername.Text = dataGridView1.Rows[e.RowIndex].Cells[14].Value.ToString();
            txtManagerEmail.Text = dataGridView1.Rows[e.RowIndex].Cells[15].Value.ToString();
            txtConfirmPassword.Enabled = false;
            txtPassword.Enabled = false;
            txtConfirmPassword.Text = "******";
            txtPassword.Text="******";
            txtPassword.Visible = txtConfirmPassword.Visible = lblPassword.Visible = lblConfirmPassword.Visible = false;
        }

        private void cmbManager_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtManagerEmail.Text = objEmp.GetEmailByEmployee(Convert.ToInt64(cmbManager.SelectedValue.ToString()));
        }
    }
}
