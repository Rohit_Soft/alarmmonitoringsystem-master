﻿namespace AlarmMonitoringSystem.Admin
{
    partial class Sections
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridViewDepartments = new System.Windows.Forms.DataGridView();
            this.SrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeptId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeptName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Section = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblHidden = new System.Windows.Forms.Label();
            this.cmbDepartment = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.txtSection = new System.Windows.Forms.TextBox();
            this.lblDepartment = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.validator1 = new Itboy.Components.Validator(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDepartments)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewDepartments
            // 
            this.dataGridViewDepartments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDepartments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SrNo,
            this.DeptId,
            this.DeptName,
            this.Section});
            this.dataGridViewDepartments.Location = new System.Drawing.Point(6, 37);
            this.dataGridViewDepartments.Name = "dataGridViewDepartments";
            this.dataGridViewDepartments.RowTemplate.Height = 28;
            this.dataGridViewDepartments.Size = new System.Drawing.Size(805, 373);
            this.dataGridViewDepartments.TabIndex = 0;
            this.dataGridViewDepartments.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewDepartments_RowHeaderMouseClick);
            // 
            // SrNo
            // 
            this.SrNo.DataPropertyName = "SrNo";
            this.SrNo.Frozen = true;
            this.SrNo.HeaderText = "Serial No";
            this.SrNo.Name = "SrNo";
            this.SrNo.ReadOnly = true;
            this.SrNo.Width = 80;
            // 
            // DeptId
            // 
            this.DeptId.DataPropertyName = "SectionId";
            this.DeptId.Frozen = true;
            this.DeptId.HeaderText = "secid";
            this.DeptId.Name = "DeptId";
            this.DeptId.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DeptId.Visible = false;
            this.DeptId.Width = 10;
            // 
            // DeptName
            // 
            this.DeptName.DataPropertyName = "Department";
            this.DeptName.Frozen = true;
            this.DeptName.HeaderText = "Department";
            this.DeptName.Name = "DeptName";
            this.DeptName.Width = 210;
            // 
            // Section
            // 
            this.Section.DataPropertyName = "Section";
            this.Section.Frozen = true;
            this.Section.HeaderText = "Section";
            this.Section.Name = "Section";
            this.Section.Width = 210;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblHidden);
            this.panel1.Controls.Add(this.cmbDepartment);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.btnInsert);
            this.panel1.Controls.Add(this.txtSection);
            this.panel1.Controls.Add(this.lblDepartment);
            this.panel1.Location = new System.Drawing.Point(25, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(803, 185);
            this.panel1.TabIndex = 6;
            // 
            // lblHidden
            // 
            this.lblHidden.AutoSize = true;
            this.lblHidden.Location = new System.Drawing.Point(14, 97);
            this.lblHidden.Name = "lblHidden";
            this.lblHidden.Size = new System.Drawing.Size(143, 20);
            this.lblHidden.TabIndex = 14;
            this.lblHidden.Text = "Select Department";
            this.lblHidden.Visible = false;
            // 
            // cmbDepartment
            // 
            this.validator1.SetComparedControl(this.cmbDepartment, this.lblHidden);
            this.validator1.SetCompareMessage(this.cmbDepartment, "Department Required.");
            this.validator1.SetCompareOperator(this.cmbDepartment, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(351, 26);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(416, 28);
            this.cmbDepartment.TabIndex = 1;
            this.validator1.SetType(this.cmbDepartment, Itboy.Components.ValidationType.Compare);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(209, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "Section";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(666, 130);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(101, 33);
            this.btnClear.TabIndex = 11;
            this.btnClear.Text = "Add";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(550, 130);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(101, 33);
            this.btnUpdate.TabIndex = 10;
            this.btnUpdate.Text = "Delete";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(434, 130);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(101, 33);
            this.btnInsert.TabIndex = 9;
            this.btnInsert.Text = "Save";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // txtSection
            // 
            this.txtSection.Location = new System.Drawing.Point(351, 72);
            this.txtSection.Name = "txtSection";
            this.validator1.SetRequiredMessage(this.txtSection, "Section Required.");
            this.txtSection.Size = new System.Drawing.Size(416, 26);
            this.txtSection.TabIndex = 2;
            this.validator1.SetType(this.txtSection, Itboy.Components.ValidationType.Required);
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            this.lblDepartment.Location = new System.Drawing.Point(178, 28);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(94, 20);
            this.lblDepartment.TabIndex = 0;
            this.lblDepartment.Text = "Department";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewDepartments);
            this.groupBox1.Location = new System.Drawing.Point(23, 195);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(805, 426);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sections";
            // 
            // validator1
            // 
            this.validator1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            // 
            // Sections
            // 
            this.AcceptButton = this.btnInsert;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 633);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Sections";
            this.Text = "Sections";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDepartments)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewDepartments;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.TextBox txtSection;
        private System.Windows.Forms.Label lblDepartment;
        private System.Windows.Forms.GroupBox groupBox1;
        private Itboy.Components.Validator validator1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbDepartment;
        private System.Windows.Forms.Label lblHidden;
        private System.Windows.Forms.DataGridViewTextBoxColumn SrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeptId;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeptName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Section;
    }
}