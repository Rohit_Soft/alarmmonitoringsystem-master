﻿using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmMonitoringSystem.Admin
{
    public partial class DefectMaster : Form
    {
        int id = 0;
        DefectBAL objDefect = new DefectBAL();
        public DefectMaster()
        {
            InitializeComponent();
            BindDefects();
            cmbCategory.SelectedIndex = 0;
        }

        private void BindDefects()
        {
            dataGridViewDefects.DataSource = objDefect.GetAllDefects();
        }

        private void dataGridViewDefects_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            id = Convert.ToInt32(dataGridViewDefects.Rows[e.RowIndex].Cells[0].Value.ToString());
            cmbCategory.SelectedIndex = cmbCategory.FindStringExact(dataGridViewDefects.Rows[e.RowIndex].Cells[2].Value.ToString());
            txtSection.Text = dataGridViewDefects.Rows[e.RowIndex].Cells[1].Value.ToString();
            pictureBox1.BackgroundImage = CommonWeb.Base64ToImage(dataGridViewDefects.Rows[e.RowIndex].Cells[3].Value.ToString());
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            id = 0;
            cmbCategory.SelectedIndex = 0;
            txtSection.Text = string.Empty;
            pictureBox1.BackgroundImage = null;
            validator1.Dispose();
            BindDefects();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (validator1.Validate())
            {
                if (pictureBox1.BackgroundImage == null)
                {
                    CommonWeb.Alert("Please select an image.", "error");
                }
                else {
                    objDefect.DefectCategory = cmbCategory.Text;
                    objDefect.DefectName = txtSection.Text;
                    objDefect.DefectId = id;
                    objDefect.Image = CommonWeb.ImageToBase64(pictureBox1.BackgroundImage, ImageFormat.Png);
                    int retValue = objDefect.InsertUpdateDefect(objDefect);

                    if (retValue == -1)
                    {
                        CommonWeb.Alert("Defect already exist.", "error");
                    }
                    else if (retValue > 0)
                    {
                        CommonWeb.Alert("Defect " + (id == 0 ? "inserted" : "updated") + " successfully.", "success");
                        Clear();
                    }
                    else
                    {
                        CommonWeb.Alert("Problem in " + (id == 0 ? "inserting" : "updating") + " Defect. Please contact administrator.", "error");
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog op1 = new OpenFileDialog();
            op1.Multiselect = false;
            op1.ShowDialog();
            op1.Filter = "allfiles|*.png";
            // File.Copy(op1.FileName, op1.FileName);
            pictureBox1.BackgroundImage = new Bitmap(op1.FileName);

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (id == 0)
            {
                CommonWeb.Alert("Please select a Defect to delete", "error");
            }
            else
            {
                int retValue = objDefect.DeleteDefect(id);
                if (retValue == -1)
                {
                    CommonWeb.Alert("Defect can't be deleted, as Setup New Job(s) depends on this Section.", "error");
                }
                else if (retValue > 0)
                {
                    CommonWeb.Alert("Defect deleted successfully.", "success");
                    Clear();
                }
                else
                {
                    CommonWeb.Alert("Problem in deleting Defect. Please contact administrator.", "error");
                }
            }
        }
    }

}
