﻿namespace AlarmMonitoringSystem
{
    partial class CreateUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateUser));
            this.pnlRegistration = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.lblNationslity = new System.Windows.Forms.Label();
            this.lblSection = new System.Windows.Forms.Label();
            this.lblDept = new System.Windows.Forms.Label();
            this.cmbNames = new System.Windows.Forms.ComboBox();
            this.cmbNationality = new System.Windows.Forms.ComboBox();
            this.cmbSection = new System.Windows.Forms.ComboBox();
            this.cmbDepartment = new System.Windows.Forms.ComboBox();
            this.lblHidden = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.txtConfirmPassword = new System.Windows.Forms.TextBox();
            this.lblConfirmPassword = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnRegister = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtFileNumber = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblFileNumber = new System.Windows.Forms.Label();
            this.cmbLoginType = new System.Windows.Forms.ComboBox();
            this.lblLoginType = new System.Windows.Forms.Label();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.validatorForm = new Itboy.Components.Validator(this.components);
            this.lblhdnDepartment = new System.Windows.Forms.Label();
            this.lblhdnName = new System.Windows.Forms.Label();
            this.lblhdnNationality = new System.Windows.Forms.Label();
            this.lblhdnSelection = new System.Windows.Forms.Label();
            this.pnlRegistration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlRegistration
            // 
            this.pnlRegistration.Controls.Add(this.label4);
            this.pnlRegistration.Controls.Add(this.lblNationslity);
            this.pnlRegistration.Controls.Add(this.lblSection);
            this.pnlRegistration.Controls.Add(this.lblDept);
            this.pnlRegistration.Controls.Add(this.cmbNames);
            this.pnlRegistration.Controls.Add(this.cmbNationality);
            this.pnlRegistration.Controls.Add(this.cmbSection);
            this.pnlRegistration.Controls.Add(this.cmbDepartment);
            this.pnlRegistration.Controls.Add(this.txtUsername);
            this.pnlRegistration.Controls.Add(this.lblUsername);
            this.pnlRegistration.Controls.Add(this.txtConfirmPassword);
            this.pnlRegistration.Controls.Add(this.lblConfirmPassword);
            this.pnlRegistration.Controls.Add(this.btnClear);
            this.pnlRegistration.Controls.Add(this.btnRegister);
            this.pnlRegistration.Controls.Add(this.txtPassword);
            this.pnlRegistration.Controls.Add(this.txtFileNumber);
            this.pnlRegistration.Controls.Add(this.lblPassword);
            this.pnlRegistration.Controls.Add(this.lblFileNumber);
            this.pnlRegistration.Controls.Add(this.cmbLoginType);
            this.pnlRegistration.Controls.Add(this.lblLoginType);
            this.pnlRegistration.Location = new System.Drawing.Point(213, 24);
            this.pnlRegistration.Name = "pnlRegistration";
            this.pnlRegistration.Size = new System.Drawing.Size(609, 528);
            this.pnlRegistration.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(98, 204);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 20);
            this.label4.TabIndex = 23;
            this.label4.Text = "Name";
            // 
            // lblNationslity
            // 
            this.lblNationslity.AutoSize = true;
            this.lblNationslity.Location = new System.Drawing.Point(67, 162);
            this.lblNationslity.Name = "lblNationslity";
            this.lblNationslity.Size = new System.Drawing.Size(82, 20);
            this.lblNationslity.TabIndex = 22;
            this.lblNationslity.Text = "Nationality";
            // 
            // lblSection
            // 
            this.lblSection.AutoSize = true;
            this.lblSection.Location = new System.Drawing.Point(86, 119);
            this.lblSection.Name = "lblSection";
            this.lblSection.Size = new System.Drawing.Size(63, 20);
            this.lblSection.TabIndex = 21;
            this.lblSection.Text = "Section";
            // 
            // lblDept
            // 
            this.lblDept.AutoSize = true;
            this.lblDept.Location = new System.Drawing.Point(55, 74);
            this.lblDept.Name = "lblDept";
            this.lblDept.Size = new System.Drawing.Size(94, 20);
            this.lblDept.TabIndex = 20;
            this.lblDept.Text = "Department";
            // 
            // cmbNames
            // 
            this.validatorForm.SetComparedControl(this.cmbNames, this.lblhdnName);
            this.validatorForm.SetCompareMessage(this.cmbNames, "Name Required");
            this.validatorForm.SetCompareOperator(this.cmbNames, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbNames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNames.FormattingEnabled = true;
            this.cmbNames.Location = new System.Drawing.Point(206, 204);
            this.cmbNames.Name = "cmbNames";
            this.cmbNames.Size = new System.Drawing.Size(336, 28);
            this.cmbNames.TabIndex = 19;
            this.validatorForm.SetType(this.cmbNames, Itboy.Components.ValidationType.Compare);
            this.cmbNames.SelectedIndexChanged += new System.EventHandler(this.cmbNames_SelectedIndexChanged);
            // 
            // cmbNationality
            // 
            this.validatorForm.SetComparedControl(this.cmbNationality, this.lblhdnNationality);
            this.validatorForm.SetCompareMessage(this.cmbNationality, "Nationality Required.");
            this.validatorForm.SetCompareOperator(this.cmbNationality, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbNationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNationality.FormattingEnabled = true;
            this.cmbNationality.Location = new System.Drawing.Point(206, 159);
            this.cmbNationality.Name = "cmbNationality";
            this.cmbNationality.Size = new System.Drawing.Size(336, 28);
            this.cmbNationality.TabIndex = 18;
            this.validatorForm.SetType(this.cmbNationality, Itboy.Components.ValidationType.Compare);
            this.cmbNationality.SelectedIndexChanged += new System.EventHandler(this.cmbNationality_SelectedIndexChanged);
            // 
            // cmbSection
            // 
            this.validatorForm.SetComparedControl(this.cmbSection, this.lblhdnSelection);
            this.validatorForm.SetCompareMessage(this.cmbSection, "Section Required.");
            this.validatorForm.SetCompareOperator(this.cmbSection, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSection.FormattingEnabled = true;
            this.cmbSection.Location = new System.Drawing.Point(206, 116);
            this.cmbSection.Name = "cmbSection";
            this.cmbSection.Size = new System.Drawing.Size(336, 28);
            this.cmbSection.TabIndex = 17;
            this.validatorForm.SetType(this.cmbSection, Itboy.Components.ValidationType.Compare);
            this.cmbSection.SelectedIndexChanged += new System.EventHandler(this.cmbSection_SelectedIndexChanged);
            // 
            // cmbDepartment
            // 
            this.validatorForm.SetComparedControl(this.cmbDepartment, this.lblhdnDepartment);
            this.validatorForm.SetCompareMessage(this.cmbDepartment, "Department Required");
            this.validatorForm.SetCompareOperator(this.cmbDepartment, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(206, 71);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(336, 28);
            this.cmbDepartment.TabIndex = 16;
            this.validatorForm.SetType(this.cmbDepartment, Itboy.Components.ValidationType.Compare);
            this.cmbDepartment.SelectedIndexChanged += new System.EventHandler(this.cmbDepartment_SelectedIndexChanged);
            // 
            // lblHidden
            // 
            this.lblHidden.AutoSize = true;
            this.lblHidden.Location = new System.Drawing.Point(12, 532);
            this.lblHidden.Name = "lblHidden";
            this.lblHidden.Size = new System.Drawing.Size(135, 20);
            this.lblHidden.TabIndex = 15;
            this.lblHidden.Text = "Select Login Type";
            this.lblHidden.Visible = false;
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(206, 290);
            this.txtUsername.MaxLength = 50;
            this.txtUsername.Name = "txtUsername";
            this.validatorForm.SetRequiredMessage(this.txtUsername, "User Name is Required.");
            this.txtUsername.Size = new System.Drawing.Size(336, 26);
            this.txtUsername.TabIndex = 2;
            this.validatorForm.SetType(this.txtUsername, Itboy.Components.ValidationType.Required);
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(60, 293);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(89, 20);
            this.lblUsername.TabIndex = 13;
            this.lblUsername.Text = "User Name";
            // 
            // txtConfirmPassword
            // 
            this.validatorForm.SetComparedControl(this.txtConfirmPassword, this.txtPassword);
            this.validatorForm.SetCompareMessage(this.txtConfirmPassword, "Password and confirm Password must be same.");
            this.validatorForm.SetCompareOperator(this.txtConfirmPassword, Itboy.Components.ValidationCompareOperator.Equal);
            this.validatorForm.SetCustomMessage(this.txtConfirmPassword, "");
            this.txtConfirmPassword.Location = new System.Drawing.Point(206, 374);
            this.txtConfirmPassword.MaxLength = 50;
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.PasswordChar = '*';
            this.validatorForm.SetRequiredMessage(this.txtConfirmPassword, "Compare Password is Required.");
            this.txtConfirmPassword.Size = new System.Drawing.Size(336, 26);
            this.txtConfirmPassword.TabIndex = 5;
            this.validatorForm.SetType(this.txtConfirmPassword, ((Itboy.Components.ValidationType)((Itboy.Components.ValidationType.Required | Itboy.Components.ValidationType.Compare))));
            // 
            // lblConfirmPassword
            // 
            this.lblConfirmPassword.AutoSize = true;
            this.lblConfirmPassword.Location = new System.Drawing.Point(12, 377);
            this.lblConfirmPassword.Name = "lblConfirmPassword";
            this.lblConfirmPassword.Size = new System.Drawing.Size(137, 20);
            this.lblConfirmPassword.TabIndex = 11;
            this.lblConfirmPassword.Text = "Confirm Password";
            // 
            // btnClear
            // 
            this.btnClear.CausesValidation = false;
            this.btnClear.Location = new System.Drawing.Point(434, 435);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(108, 33);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(279, 435);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(108, 33);
            this.btnRegister.TabIndex = 7;
            this.btnRegister.Text = "Register";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(206, 332);
            this.txtPassword.MaxLength = 50;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.validatorForm.SetRequiredMessage(this.txtPassword, "Password is Required.");
            this.txtPassword.Size = new System.Drawing.Size(336, 26);
            this.txtPassword.TabIndex = 4;
            this.validatorForm.SetType(this.txtPassword, Itboy.Components.ValidationType.Required);
            // 
            // txtFileNumber
            // 
            this.validatorForm.SetCompareMessage(this.txtFileNumber, "File Number must be numeric.");
            this.validatorForm.SetDataType(this.txtFileNumber, Itboy.Components.ValidationDataType.Integer);
            this.txtFileNumber.Location = new System.Drawing.Point(206, 248);
            this.txtFileNumber.MaxLength = 50;
            this.txtFileNumber.Name = "txtFileNumber";
            this.txtFileNumber.ReadOnly = true;
            this.validatorForm.SetRequiredMessage(this.txtFileNumber, "File Number is Required.");
            this.txtFileNumber.Size = new System.Drawing.Size(336, 26);
            this.txtFileNumber.TabIndex = 3;
            this.validatorForm.SetType(this.txtFileNumber, ((Itboy.Components.ValidationType)((Itboy.Components.ValidationType.Required | Itboy.Components.ValidationType.Compare))));
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(71, 335);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(78, 20);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Password";
            // 
            // lblFileNumber
            // 
            this.lblFileNumber.AutoSize = true;
            this.lblFileNumber.Location = new System.Drawing.Point(55, 251);
            this.lblFileNumber.Name = "lblFileNumber";
            this.lblFileNumber.Size = new System.Drawing.Size(94, 20);
            this.lblFileNumber.TabIndex = 2;
            this.lblFileNumber.Text = "File Number";
            // 
            // cmbLoginType
            // 
            this.validatorForm.SetComparedControl(this.cmbLoginType, this.lblHidden);
            this.validatorForm.SetCompareMessage(this.cmbLoginType, "Login Type Required.");
            this.validatorForm.SetCompareOperator(this.cmbLoginType, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.validatorForm.SetCustomMessage(this.cmbLoginType, "");
            this.cmbLoginType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoginType.FormattingEnabled = true;
            this.cmbLoginType.Location = new System.Drawing.Point(206, 26);
            this.cmbLoginType.Name = "cmbLoginType";
            this.validatorForm.SetRangeMessage(this.cmbLoginType, "");
            this.validatorForm.SetRangeOfLowerBound(this.cmbLoginType, "");
            this.validatorForm.SetRangeOfUpperBound(this.cmbLoginType, "");
            this.validatorForm.SetRequiredMessage(this.cmbLoginType, "Login Type Required.");
            this.cmbLoginType.Size = new System.Drawing.Size(336, 28);
            this.cmbLoginType.TabIndex = 0;
            this.validatorForm.SetType(this.cmbLoginType, ((Itboy.Components.ValidationType)((Itboy.Components.ValidationType.Required | Itboy.Components.ValidationType.Compare))));
            // 
            // lblLoginType
            // 
            this.lblLoginType.AutoSize = true;
            this.lblLoginType.Location = new System.Drawing.Point(63, 29);
            this.lblLoginType.Name = "lblLoginType";
            this.lblLoginType.Size = new System.Drawing.Size(86, 20);
            this.lblLoginType.TabIndex = 0;
            this.lblLoginType.Text = "Login Type";
            // 
            // picLogo
            // 
            this.picLogo.Image = ((System.Drawing.Image)(resources.GetObject("picLogo.Image")));
            this.picLogo.Location = new System.Drawing.Point(25, 95);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(156, 174);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 2;
            this.picLogo.TabStop = false;
            // 
            // validatorForm
            // 
            this.validatorForm.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.validatorForm.Form = this;
            // 
            // lblhdnDepartment
            // 
            this.lblhdnDepartment.AutoSize = true;
            this.lblhdnDepartment.Location = new System.Drawing.Point(64, 448);
            this.lblhdnDepartment.Name = "lblhdnDepartment";
            this.lblhdnDepartment.Size = new System.Drawing.Size(143, 20);
            this.lblhdnDepartment.TabIndex = 24;
            this.lblhdnDepartment.Text = "Select Department";
            this.lblhdnDepartment.Visible = false;
            // 
            // lblhdnName
            // 
            this.lblhdnName.AutoSize = true;
            this.lblhdnName.Location = new System.Drawing.Point(37, 381);
            this.lblhdnName.Name = "lblhdnName";
            this.lblhdnName.Size = new System.Drawing.Size(100, 20);
            this.lblhdnName.TabIndex = 25;
            this.lblhdnName.Text = "Select Name";
            this.lblhdnName.Visible = false;
            // 
            // lblhdnNationality
            // 
            this.lblhdnNationality.AutoSize = true;
            this.lblhdnNationality.Location = new System.Drawing.Point(37, 401);
            this.lblhdnNationality.Name = "lblhdnNationality";
            this.lblhdnNationality.Size = new System.Drawing.Size(131, 20);
            this.lblhdnNationality.TabIndex = 26;
            this.lblhdnNationality.Text = "Select Nationality";
            this.lblhdnNationality.Visible = false;
            // 
            // lblhdnSelection
            // 
            this.lblhdnSelection.AutoSize = true;
            this.lblhdnSelection.Location = new System.Drawing.Point(37, 496);
            this.lblhdnSelection.Name = "lblhdnSelection";
            this.lblhdnSelection.Size = new System.Drawing.Size(112, 20);
            this.lblhdnSelection.TabIndex = 27;
            this.lblhdnSelection.Text = "Select Section";
            this.lblhdnSelection.Visible = false;
            // 
            // CreateUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 566);
            this.Controls.Add(this.lblhdnName);
            this.Controls.Add(this.lblhdnSelection);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.lblhdnNationality);
            this.Controls.Add(this.lblhdnDepartment);
            this.Controls.Add(this.pnlRegistration);
            this.Controls.Add(this.lblHidden);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateUser";
            this.Text = "Registration for Login";
            this.pnlRegistration.ResumeLayout(false);
            this.pnlRegistration.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlRegistration;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblFileNumber;
        private System.Windows.Forms.ComboBox cmbLoginType;
        private System.Windows.Forms.Label lblLoginType;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.TextBox txtFileNumber;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.Label lblConfirmPassword;
        private Itboy.Components.Validator validatorForm;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblHidden;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblNationslity;
        private System.Windows.Forms.Label lblSection;
        private System.Windows.Forms.Label lblDept;
        private System.Windows.Forms.ComboBox cmbSection;
        private System.Windows.Forms.ComboBox cmbDepartment;
        private System.Windows.Forms.ComboBox cmbNationality;
        private System.Windows.Forms.ComboBox cmbNames;
        private System.Windows.Forms.Label lblhdnSelection;
        private System.Windows.Forms.Label lblhdnNationality;
        private System.Windows.Forms.Label lblhdnName;
        private System.Windows.Forms.Label lblhdnDepartment;
    }
}

