﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AlarmMonitoringClassLibrary;
using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;

namespace AlarmMonitoringSystem.Admin
{
    public partial class Sections : Form
    {
        SectionBAL objSection = new SectionBAL();
        DepartmentBAL objDept = new DepartmentBAL();
        int id = 0;
        public Sections()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            BindDepartments();
            BindSections();
        }

        private void BindSections()
        {
            //dataGridViewDepartments.AutoGenerateColumns = false;
            dataGridViewDepartments.DataSource = objSection.GetAllSections();

        }
        private void BindDepartments()
        {
            cmbDepartment.ValueMember = "DepartmentId";
            cmbDepartment.DisplayMember = "Department";
            cmbDepartment.DataSource = objDept.GetAllDepartments(true);
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            InsertUpdateSection();
        }

        private void InsertUpdateSection()
        {
            if (validator1.Validate())
            {
                objSection.DepartmentId = Convert.ToInt32(cmbDepartment.SelectedValue.ToString());
                objSection.Section = txtSection.Text;
                objSection.SectionId = id;
                int retValue = objSection.InsertUpdateSections(objSection);

                if (retValue == -1)
                {
                    CommonWeb.Alert("Section already exist.", "error");
                }
                else if (retValue > 0)
                {
                    CommonWeb.Alert("Section " + (id == 0 ? "inserted" : "updated") + " successfully.", "success");
                    Clear();
                }
                else
                {
                    CommonWeb.Alert("Problem in " + (id == 0 ? "inserting" : "updating") + " Section. Please contact administrator.", "error");
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (id == 0)
            {
                CommonWeb.Alert("Please select a Section to delete", "error");
            }
            else
            {
                int retValue = objSection.DeleteSection(id);
                if (retValue == -1)
                {
                    CommonWeb.Alert("Section can't be deleted, as Employee(s) depends on this Section.", "error");
                }
                else if (retValue > 0)
                {
                    CommonWeb.Alert("Section deleted successfully.", "success");
                    Clear();
                }
                else
                {
                    CommonWeb.Alert("Problem in deleting Section. Please contact administrator.", "error");
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            cmbDepartment.SelectedIndex = 0;
            txtSection.Text = string.Empty;
            id = 0;
            validator1.Dispose();
            BindSections();
        }

        private void dataGridViewDepartments_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            id = Convert.ToInt32(dataGridViewDepartments.Rows[e.RowIndex].Cells[1].Value.ToString());
            cmbDepartment.SelectedIndex = cmbDepartment.FindStringExact(dataGridViewDepartments.Rows[e.RowIndex].Cells[3].Value.ToString());
            txtSection.Text = dataGridViewDepartments.Rows[e.RowIndex].Cells[2].Value.ToString();
        }
    }
}
