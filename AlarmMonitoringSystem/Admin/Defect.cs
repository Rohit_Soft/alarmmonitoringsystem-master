﻿using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmMonitoringSystem.Admin
{
    public partial class Defect : Form
    {
        public static string DefectIds = "", tempDefects = "";
        DefectBAL objDefect = new DefectBAL();
       
        public Defect()
        {
            InitializeComponent();
            tempDefects = DefectIds;
            BindDefects("Critical", tabPage1);
            BindDefects("Major", tabPage2);
            BindDefects("Minor", tabPage3);

            string[] defectArray = tempDefects.Split(',');
            for (int i = 0; i < defectArray.Length; i++)
            {
                foreach (TabPage tabPage in tabControl1.TabPages)
                {
                    foreach (Panel pnl in tabPage.Controls)
                    {
                        foreach (Control lbl in pnl.Controls)
                        {
                            if (lbl.GetType() == typeof(Label))
                            {
                                if (((Label)lbl).Text == defectArray[i])
                                {
                                    foreach (Control chk in pnl.Controls)
                                    {
                                        if (chk.GetType() == typeof(CheckBox))
                                        {
                                            ((CheckBox)chk).Checked = true;
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }

        private void BindDefects(string type, Control parentControl)
        {
            DataTable dt = new DataTable();
            dt = objDefect.GetAllDefects(type);
            int pointX = 30;
            int pointY = 20;
            int i = 1;
            foreach (DataRow dr in dt.Rows)
            {
                Panel pnl = new Panel();
                pnl.Name = "pnl" + i;
                pnl.Height = 120;
                pnl.Width = 160;
                pnl.Location = new Point(pointX, pointY);
                pnl.BorderStyle = BorderStyle.Fixed3D;
                CheckBox chk = new CheckBox();
                chk.Name = "chk" + i;
                chk.Appearance = Appearance.Button;
                chk.BackgroundImage = CommonWeb.Base64ToImage(dr["Image"].ToString());
                chk.BackgroundImageLayout = ImageLayout.Stretch;
                chk.Height = 100;
                chk.Width = 150;
                // chk.Text = dr["DefectName"].ToString();
                // chk.TextImageRelation = TextImageRelation.ImageAboveText;
                // chk.ForeColor = Color.Black;
                // chk.TextAlign = ContentAlignment.BottomCenter;
                chk.CheckedChanged += Chk_CheckedChanged;
               // ControlMover.Init(pnl);
                pnl.Controls.Add(chk);
               // ControlMover.Init(chk);
                TextBox txt = new TextBox();
                txt.Name = "txt" + i;
                txt.Text = dr["DefectName"].ToString();
                txt.ForeColor = Color.Black;
                txt.ReadOnly = true;
                txt.BorderStyle = BorderStyle.None;
                txt.Location = new Point(0, 100);
                txt.Width = 150;
                txt.TextAlign = HorizontalAlignment.Center;
                
                pnl.Controls.Add(txt);
                //ControlMover.Init(txt);
                Label lbl = new Label();
                lbl.Name = "lbl" + i;
                lbl.Text = dr["DefectId"].ToString();
                pnl.Controls.Add(lbl);
                pointX += 200;
                if (i % 3 == 0)
                {
                    pointX = 30;
                    pointY += 140;
                }

                parentControl.Controls.Add(pnl);
                
                i++;
            }
        }

        private void Chk_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
            {
                ((CheckBox)sender).BackColor = System.Drawing.Color.Red;
                ((CheckBox)sender).FlatStyle = FlatStyle.Flat;
                ((CheckBox)sender).FlatAppearance.BorderColor = System.Drawing.Color.OrangeRed;
                ((CheckBox)sender).FlatAppearance.BorderSize = 2;
            }
            else
            {
                ((CheckBox)sender).FlatAppearance.BorderSize = 0;
            }
            DefectIds = "";
            foreach (TabPage tabPage in tabControl1.TabPages)
            {
                foreach (Panel pnl in tabPage.Controls)
                {
                    foreach (Control chk in pnl.Controls)
                    {
                        if (chk.GetType() == typeof(CheckBox) && ((CheckBox)chk).Checked)
                        {
                            foreach (Control lbl in pnl.Controls)
                            {
                                if (lbl.GetType() == typeof(Label))
                                {
                                    DefectIds += ((Label)lbl).Text + ",";
                                }
                            }
                        }
                    }
                }
            }

            //MessageBox.Show(DefectIds.TrimEnd(','));
        }



        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            DefectIds = tempDefects;
            this.Close();
        }
    }
}
