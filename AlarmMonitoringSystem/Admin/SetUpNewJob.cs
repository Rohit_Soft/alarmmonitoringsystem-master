﻿using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmMonitoringSystem.Admin
{
    public partial class SetUpNewJob : Form
    {
        int id = 0;
        SetupNewJobBAL objSetupNewJob = new SetupNewJobBAL();
        public SetUpNewJob()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            BindCustomers();
            BindArticleCategory();
            BindDefects();
            BindProductOrderNumber();
            dataGridView1.DataSource = objSetupNewJob.GetAllSetupJobs();
        }
        private void BindCustomers()
        { }
        private void BindArticleCategory()
        {
            cmbArticleCategory.ValueMember = "ArticleCategoryId";
            cmbArticleCategory.DisplayMember = "ArticleCategory";
            cmbArticleCategory.DataSource = objSetupNewJob.GetAllArticleCategory();
        }
        private void BindDefects()
        { }

        private void BindProductOrderNumber()
        {
            cmbProductOrderNumber.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cmbProductOrderNumber.AutoCompleteSource = AutoCompleteSource.ListItems;

            cmbProductOrderNumber.ValueMember = "ProductionOrderId";
            cmbProductOrderNumber.DisplayMember = "ProductionOrderNumber";
            cmbProductOrderNumber.DataSource = objSetupNewJob.AutoCompleterProductOrderNumber(Convert.ToInt32(cmbArticleCategory.SelectedValue));
        }
      
        private void Clear()
        {
            validator1.Dispose();
            id = 0;
            txtLineNumber.Text = string.Empty;
            cmbArticleCategory.SelectedIndex = 0;
            cmbProductOrderNumber.SelectedIndex = 0;
            txtArticleNumer.Text = string.Empty;
            txtArticleName.Text = string.Empty;
            txtCustomer.Text = string.Empty; 
            txtQty.Text = string.Empty;
            txtEfficiency.Text = string.Empty;
            txtLineSpeed.Text = string.Empty;
            txtArticlePerMinute.Text = string.Empty;
            txtApproxDays.Text = string.Empty;
            Defect.DefectIds = "";
            dataGridView1.DataSource = objSetupNewJob.GetAllSetupJobs();
            //cmbDefectSelection.SelectedIndex = 0;
        }

        private void cmbArticleCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProductOrderNumber();
        }

        private void btnDefects_Click(object sender, EventArgs e)
        {
            Defect defectPopup = new Admin.Defect();
            defectPopup.ShowDialog();
        }

       

        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (validator1.Validate())
            {
                objSetupNewJob.NewJobId = id;
                objSetupNewJob.LineNumber = txtLineNumber.Text;
                objSetupNewJob.ArticleCategory = Convert.ToInt32(cmbArticleCategory.SelectedValue);
                objSetupNewJob.ProductionOrderId = Convert.ToInt32(cmbProductOrderNumber.SelectedValue);
                objSetupNewJob.ArticleNumber = txtArticleNumer.Text;
                objSetupNewJob.ArticleName = txtArticleName.Text;
                objSetupNewJob.Customer = txtCustomer.Text;
                objSetupNewJob.Qty = Convert.ToInt32(txtQty.Text);
                objSetupNewJob.Efficiency = txtEfficiency.Text;
                objSetupNewJob.LineSpeed = txtLineSpeed.Text;
                objSetupNewJob.ArticlePerMinute = txtArticlePerMinute.Text;
                objSetupNewJob.ApproxDays = txtApproxDays.Text;
                // objSetupNewJob.DefectSelection = Convert.ToInt32(cmbDefectSelection.SelectedValue);
                objSetupNewJob.DefectSelection = Defect.DefectIds.TrimEnd(',');
                int retValue = objSetupNewJob.InsertUpdateSetupNewJob(objSetupNewJob);
                if (retValue > 0)
                {
                    CommonWeb.Alert("Job " + (id == 0 ? "inserted" : "updated") + " successfully.", "success");
                    Clear();
                }
                else
                {
                    CommonWeb.Alert("Problem in " + (id == 0 ? "inserting" : "updating") + " Job. Please contact administrator.", "error");
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (id == 0)
            {
                CommonWeb.Alert("Please select a Job to delete", "error");
            }
            else {
               
                int retValue = objSetupNewJob.DeleteSetupNewJob(id);
                if (retValue > 0)
                {
                    CommonWeb.Alert("Job deleted successfully.", "success");
                    Clear();
                }
                else
                {
                    CommonWeb.Alert("Problem in deleting Job. Please contact administrator.", "error");
                }
            }
        }

        private void dataGridView1_RowHeaderMouseClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
            id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtLineNumber.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            cmbArticleCategory.SelectedIndex = cmbArticleCategory.FindStringExact(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());
            cmbProductOrderNumber.SelectedIndex = cmbProductOrderNumber.FindStringExact(dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString());
            txtArticleNumer.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            txtArticleName.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtCustomer.Text = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtQty.Text = dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtEfficiency.Text = dataGridView1.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtLineSpeed.Text = dataGridView1.Rows[e.RowIndex].Cells[9].Value.ToString();
            txtArticlePerMinute.Text = dataGridView1.Rows[e.RowIndex].Cells[10].Value.ToString();
            txtApproxDays.Text = dataGridView1.Rows[e.RowIndex].Cells[11].Value.ToString();
            Defect.DefectIds = dataGridView1.Rows[e.RowIndex].Cells[12].Value.ToString();

        }
    }
}
