﻿using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace AlarmMonitoringSystem.Admin
{
    public partial class DefectBkp : Form
    {
        public static string DefectIds = "";
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefectBkp));

        DefectBAL objDefect = new DefectBAL();

        public DefectBkp()
        {
            InitializeComponent();
            BindDefects();
            string[] defectArray = DefectIds.Split(',');
            for (int i = 0; i < defectArray.Length; i++)
            {
                foreach (Panel pnl in panel1.Controls)
                {
                    foreach (Control lbl in pnl.Controls)
                    {
                        if (lbl.GetType() == typeof(Label))
                        {
                            if (((Label)lbl).Text == defectArray[i])
                            {
                                foreach (Control chk in pnl.Controls)
                                {
                                    if (chk.GetType() == typeof(CheckBox))
                                    {
                                        ((CheckBox)chk).Checked = true;
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
        private void BindDefects()
        {
            DataTable dt = new DataTable();
            dt = objDefect.GetAllDefects();
            int pointX = 30;
            int pointY = 20;
            int i = 1;
            foreach (DataRow dr in dt.Rows)
            {
                Panel pnl = new Panel();
                pnl.Name = "pnl" + i;
                pnl.Height = 120;
                pnl.Width = 160;
                pnl.Location = new Point(pointX, pointY);

                CheckBox chk = new CheckBox();
                chk.Name = "chk" + i;
                chk.Appearance = Appearance.Button;
                chk.BackgroundImage = Base64ToImage(dr["Image"].ToString());
                chk.BackgroundImageLayout = ImageLayout.Stretch;
                chk.Height = 100;
                chk.Width = 150;
               // chk.Text = dr["DefectName"].ToString();
               // chk.TextImageRelation = TextImageRelation.ImageAboveText;
               // chk.ForeColor = Color.Black;
               // chk.TextAlign = ContentAlignment.BottomCenter;
                chk.CheckedChanged += Chk_CheckedChanged;
                pnl.Controls.Add(chk);
                TextBox txt = new TextBox();
                txt.Name = "txt" + i;
                txt.Text = dr["DefectName"].ToString();
                txt.ForeColor = Color.Black;
                txt.ReadOnly = true;
                txt.BorderStyle = BorderStyle.None;
                txt.Location = new Point(0,100 );
                txt.Width = 150;
                txt.TextAlign = HorizontalAlignment.Center;
                pnl.Controls.Add(txt);
                Label lbl = new Label();
                lbl.Name = "lbl" + i;
                lbl.Text = dr["DefectId"].ToString();
                pnl.Controls.Add(lbl);
                pointY += 120;
                if (i % 3 == 0)
                {
                    pointX += 200;
                    pointY = 20;
                }

                panel1.Controls.Add(pnl);
                i++;
            }
        }

        private void Chk_CheckedChanged(object sender, EventArgs e)
        {
            DefectIds = "";
            foreach (Panel pnl in panel1.Controls)
            {
                foreach (Control chk in pnl.Controls)
                {
                    if (chk.GetType() == typeof(CheckBox) && ((CheckBox)chk).Checked)
                    {
                        foreach (Control lbl in pnl.Controls)
                        {
                            if (lbl.GetType() == typeof(Label))
                            {
                                DefectIds += ((Label)lbl).Text + ",";
                            }

                        }
                    }
                }
            }

            //MessageBox.Show(DefectIds.TrimEnd(','));
        }

        public Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }
    }
}



