﻿namespace AlarmMonitoringSystem.Admin
{
    partial class DefectMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridViewDefects = new System.Windows.Forms.DataGridView();
            this.DeptId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DefectCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DefectName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Image = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblHidden = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.txtSection = new System.Windows.Forms.TextBox();
            this.lblDepartment = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.validator1 = new Itboy.Components.Validator(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDefects)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewDefects
            // 
            this.dataGridViewDefects.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDefects.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DeptId,
            this.DefectCategory,
            this.DefectName,
            this.Image});
            this.dataGridViewDefects.Location = new System.Drawing.Point(6, 37);
            this.dataGridViewDefects.Name = "dataGridViewDefects";
            this.dataGridViewDefects.RowTemplate.Height = 28;
            this.dataGridViewDefects.Size = new System.Drawing.Size(805, 373);
            this.dataGridViewDefects.TabIndex = 0;
            this.dataGridViewDefects.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewDefects_RowHeaderMouseClick);
            // 
            // DeptId
            // 
            this.DeptId.DataPropertyName = "DefectId";
            this.DeptId.Frozen = true;
            this.DeptId.HeaderText = "secid";
            this.DeptId.Name = "DeptId";
            this.DeptId.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DeptId.Visible = false;
            this.DeptId.Width = 10;
            // 
            // DefectCategory
            // 
            this.DefectCategory.DataPropertyName = "DefectCategory";
            this.DefectCategory.Frozen = true;
            this.DefectCategory.HeaderText = "Category";
            this.DefectCategory.Name = "DefectCategory";
            this.DefectCategory.Width = 210;
            // 
            // DefectName
            // 
            this.DefectName.DataPropertyName = "DefectName";
            this.DefectName.Frozen = true;
            this.DefectName.HeaderText = "Defect";
            this.DefectName.Name = "DefectName";
            this.DefectName.Width = 210;
            // 
            // Image
            // 
            this.Image.DataPropertyName = "Image";
            this.Image.Frozen = true;
            this.Image.HeaderText = "Image";
            this.Image.Name = "Image";
            this.Image.ReadOnly = true;
            this.Image.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblHidden);
            this.panel1.Controls.Add(this.cmbCategory);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.btnInsert);
            this.panel1.Controls.Add(this.txtSection);
            this.panel1.Controls.Add(this.lblDepartment);
            this.panel1.Location = new System.Drawing.Point(7, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(803, 185);
            this.panel1.TabIndex = 8;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(273, 59);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 100);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(140, 59);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 33);
            this.button1.TabIndex = 16;
            this.button1.Text = "Upload...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 20);
            this.label2.TabIndex = 15;
            this.label2.Text = "Image";
            // 
            // lblHidden
            // 
            this.lblHidden.AutoSize = true;
            this.lblHidden.Location = new System.Drawing.Point(667, 107);
            this.lblHidden.Name = "lblHidden";
            this.lblHidden.Size = new System.Drawing.Size(122, 20);
            this.lblHidden.TabIndex = 14;
            this.lblHidden.Text = "Select Category";
            this.lblHidden.Visible = false;
            // 
            // cmbCategory
            // 
            this.validator1.SetComparedControl(this.cmbCategory, this.lblHidden);
            this.validator1.SetCompareMessage(this.cmbCategory, "Category Required.");
            this.validator1.SetCompareOperator(this.cmbCategory, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Items.AddRange(new object[] {
            "Select Category",
            "Critical",
            "Major",
            "Minor"});
            this.cmbCategory.Location = new System.Drawing.Point(140, 8);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(185, 28);
            this.cmbCategory.TabIndex = 1;
            this.validator1.SetType(this.cmbCategory, Itboy.Components.ValidationType.Compare);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(385, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "Defect";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(674, 143);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(101, 33);
            this.btnClear.TabIndex = 11;
            this.btnClear.Text = "Add";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(558, 143);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(101, 33);
            this.btnUpdate.TabIndex = 10;
            this.btnUpdate.Text = "Delete";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(442, 143);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(101, 33);
            this.btnInsert.TabIndex = 9;
            this.btnInsert.Text = "Save";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // txtSection
            // 
            this.txtSection.Location = new System.Drawing.Point(477, 8);
            this.txtSection.Name = "txtSection";
            this.validator1.SetRequiredMessage(this.txtSection, "Defect Required.");
            this.txtSection.Size = new System.Drawing.Size(293, 26);
            this.txtSection.TabIndex = 2;
            this.validator1.SetType(this.txtSection, Itboy.Components.ValidationType.Required);
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            this.lblDepartment.Location = new System.Drawing.Point(23, 16);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(73, 20);
            this.lblDepartment.TabIndex = 0;
            this.lblDepartment.Text = "Category";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewDefects);
            this.groupBox1.Location = new System.Drawing.Point(5, 187);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(805, 426);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Defects";
            // 
            // validator1
            // 
            this.validator1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // DefectMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 608);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DefectMaster";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Defects";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDefects)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewDefects;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblHidden;
        private System.Windows.Forms.ComboBox cmbCategory;
        private Itboy.Components.Validator validator1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.TextBox txtSection;
        private System.Windows.Forms.Label lblDepartment;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeptId;
        private System.Windows.Forms.DataGridViewTextBoxColumn DefectCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn DefectName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Image;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}