﻿using AlarmMonitoringClassLibrary;
using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace AlarmMonitoringSystem
{
    public partial class CreateUser : Form
    {
        RegistrationBAL obj = new RegistrationBAL();
        DepartmentBAL objDept = new DepartmentBAL();
        EmployeeBAL objEmp = new EmployeeBAL();
        public CreateUser()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            BindLoginType();
            BindNationality();
            BindDepartments();
            BindSection();
          
            BindNames();
        }

        private void BindLoginType()
        {
            cmbLoginType.ValueMember = "LoginTypeId";
            cmbLoginType.DisplayMember = "LoginType";
            cmbLoginType.DataSource = obj.GetLoginTypes();
        }

        private void BindDepartments()
        {
            cmbDepartment.ValueMember = "DepartmentId";
            cmbDepartment.DisplayMember = "Department";
            cmbDepartment.DataSource = objDept.GetAllDepartments(true);
        }
        private void BindSection()
        {
            SectionBAL obj = new SectionBAL();
            cmbSection.ValueMember = "SectionId";
            cmbSection.DisplayMember = "Section";
            cmbSection.DataSource = obj.GetAllSectionsByDepartment(Convert.ToInt32(cmbDepartment.SelectedValue.ToString()));
        }

        private void BindNationality()
        {
            cmbNationality.ValueMember = "NationalityId";
            cmbNationality.DisplayMember = "Nationality";
            cmbNationality.DataSource = objEmp.GetAllNationality();
        }

        private void BindNames()
        {
            if (cmbDepartment.SelectedValue != null && cmbSection.SelectedValue != null && cmbNationality.SelectedValue != null)
            {
                cmbNames.ValueMember = "EmployeeId";
                cmbNames.DisplayMember = "Name";
                cmbNames.DataSource = obj.GetAllNames(Convert.ToInt32(cmbDepartment.SelectedValue.ToString()), Convert.ToInt32(cmbSection.SelectedValue.ToString()), Convert.ToInt32(cmbNationality.SelectedValue.ToString()));
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            cmbLoginType.SelectedIndex = 0;
            cmbDepartment.SelectedIndex = 0;
            cmbSection.SelectedIndex = 0;
            cmbNationality.SelectedIndex = 0;
            cmbNames.SelectedIndex = 0;
            txtFileNumber.Text = string.Empty;
            txtUsername.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtConfirmPassword.Text = string.Empty;
            validatorForm.Dispose();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (validatorForm.Validate())
            {
                obj.LoginTypeId = Convert.ToInt32(cmbLoginType.SelectedValue);
                obj.EmployeeId = Convert.ToInt32(cmbNames.SelectedValue);
                obj.Username = txtUsername.Text;
                obj.FileNumber = txtFileNumber.Text;
                obj.Password = Encrypt.EncryptString(txtPassword.Text);
                int retValue = obj.RegisterUser(obj);

                if (retValue == -1)
                {
                    CommonWeb.Alert("User Name already exist.", "error");
                }
                else if (retValue == -2)
                {
                    CommonWeb.Alert("File Number already exist.", "error");
                }
                else if (retValue > 0)
                {
                    CommonWeb.Alert("User Registered successfully.", "success");
                    Clear();
                }
                else
                {
                    CommonWeb.Alert("Problem in Registering User. Please contact administrator.", "error");
                }
            }
        }

        private void cmbDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSection();
            BindNames();
        }

        private void cmbSection_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindNames();
        }

        private void cmbNationality_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindNames();
        }

        private void cmbNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtFileNumber.Text = objEmp.GetFileNumberByEmployee(Convert.ToInt32(cmbNames.SelectedValue));
        }
    }
}
