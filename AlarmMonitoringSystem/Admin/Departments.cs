﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AlarmMonitoringClassLibrary;
using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;

namespace AlarmMonitoringSystem.Admin
{
    public partial class Departments : Form
    {
        DepartmentBAL objDept = new DepartmentBAL();
        int id = 0;
        public Departments()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            BindDepartments();
        }

        private void BindDepartments()
        {
            //dataGridViewDepartments.AutoGenerateColumns = false;
            dataGridViewDepartments.DataSource = objDept.GetAllDepartments(false);
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            InsertUpdateDepartment();
        }

        private void Clear()
        {
            txtDepartment.Text = string.Empty;
            id = 0;
            validator1.Dispose();
            BindDepartments();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (id == 0)
            {
                CommonWeb.Alert("Please select a Department to delete", "error");
            }
            else {
                int retValue = objDept.DeleteDepartment(id);
                if (retValue == -1)
                {
                    CommonWeb.Alert("Department can't be deleted, as Section(s) depends on this Department.", "error");
                }
                else if (retValue == -2)
                {
                    CommonWeb.Alert("Department can't be deleted, as Employee(s) depends on this Department.", "error");
                }
                else if (retValue > 0)
                {
                    CommonWeb.Alert("Department deleted successfully.", "success");
                    Clear();
                }
                else
                {
                    CommonWeb.Alert("Problem in deleting Department. Please contact administrator.", "error");
                }
            }
        }

        private void InsertUpdateDepartment()
        {
            if (validator1.Validate())
            {
                objDept.DepartmentId = id;
                objDept.Department = txtDepartment.Text;
                int retValue = objDept.InsertUpdateDepartments(objDept);

                if (retValue == -1)
                {
                    CommonWeb.Alert("Department already exist.", "error");
                }
                else if (retValue > 0)
                {
                    CommonWeb.Alert("Department " + (id == 0 ? "inserted" : "updated") + " successfully.", "success");
                    Clear();
                }
                else
                {
                    CommonWeb.Alert("Problem in " + (id == 0 ? "inserting" : "updating") + " Department. Please contact administrator.", "error");
                }
            }
        }

        private void dataGridViewDepartments_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            id = Convert.ToInt32(dataGridViewDepartments.Rows[e.RowIndex].Cells[2].Value.ToString());
            txtDepartment.Text = dataGridViewDepartments.Rows[e.RowIndex].Cells[1].Value.ToString();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }
    }
}
