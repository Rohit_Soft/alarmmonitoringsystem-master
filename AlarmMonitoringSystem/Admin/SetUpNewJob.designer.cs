﻿namespace AlarmMonitoringSystem.Admin
{
    partial class SetUpNewJob
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblhdnProductOrderNumber = new System.Windows.Forms.Label();
            this.lblhdntarticlecategory = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.btnDefects = new System.Windows.Forms.Button();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.cmbProductOrderNumber = new System.Windows.Forms.ComboBox();
            this.txtApproxDays = new System.Windows.Forms.TextBox();
            this.txtArticlePerMinute = new System.Windows.Forms.TextBox();
            this.txtLineSpeed = new System.Windows.Forms.TextBox();
            this.txtEfficiency = new System.Windows.Forms.TextBox();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.txtArticleName = new System.Windows.Forms.TextBox();
            this.txtArticleNumer = new System.Windows.Forms.TextBox();
            this.cmbArticleCategory = new System.Windows.Forms.ComboBox();
            this.txtLineNumber = new System.Windows.Forms.TextBox();
            this.lblDefectSelection = new System.Windows.Forms.Label();
            this.lblApproxDay = new System.Windows.Forms.Label();
            this.lblArticlePerMinute = new System.Windows.Forms.Label();
            this.lblLineSpeed = new System.Windows.Forms.Label();
            this.lblEfficiency = new System.Windows.Forms.Label();
            this.lblQty = new System.Windows.Forms.Label();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.lblArticleName = new System.Windows.Forms.Label();
            this.lblArticleNumber = new System.Windows.Forms.Label();
            this.lblProdOrderNo = new System.Windows.Forms.Label();
            this.lblArticleCategory = new System.Windows.Forms.Label();
            this.lblLineNumber = new System.Windows.Forms.Label();
            this.validator1 = new Itboy.Components.Validator(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.NewJobId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LineNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArticleCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductionOrderNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArticleNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArticleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Customer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Efficiency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LineSpeed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArticlePerMinute = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ApproxDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DefectIds = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DefectName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblhdnProductOrderNumber);
            this.panel1.Controls.Add(this.lblhdntarticlecategory);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Controls.Add(this.btnInsert);
            this.panel1.Controls.Add(this.btnDefects);
            this.panel1.Controls.Add(this.txtCustomer);
            this.panel1.Controls.Add(this.cmbProductOrderNumber);
            this.panel1.Controls.Add(this.txtApproxDays);
            this.panel1.Controls.Add(this.txtArticlePerMinute);
            this.panel1.Controls.Add(this.txtLineSpeed);
            this.panel1.Controls.Add(this.txtEfficiency);
            this.panel1.Controls.Add(this.txtQty);
            this.panel1.Controls.Add(this.txtArticleName);
            this.panel1.Controls.Add(this.txtArticleNumer);
            this.panel1.Controls.Add(this.cmbArticleCategory);
            this.panel1.Controls.Add(this.txtLineNumber);
            this.panel1.Controls.Add(this.lblDefectSelection);
            this.panel1.Controls.Add(this.lblApproxDay);
            this.panel1.Controls.Add(this.lblArticlePerMinute);
            this.panel1.Controls.Add(this.lblLineSpeed);
            this.panel1.Controls.Add(this.lblEfficiency);
            this.panel1.Controls.Add(this.lblQty);
            this.panel1.Controls.Add(this.lblCustomer);
            this.panel1.Controls.Add(this.lblArticleName);
            this.panel1.Controls.Add(this.lblArticleNumber);
            this.panel1.Controls.Add(this.lblProdOrderNo);
            this.panel1.Controls.Add(this.lblArticleCategory);
            this.panel1.Controls.Add(this.lblLineNumber);
            this.panel1.Location = new System.Drawing.Point(16, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1106, 389);
            this.panel1.TabIndex = 0;
            // 
            // lblhdnProductOrderNumber
            // 
            this.lblhdnProductOrderNumber.AutoSize = true;
            this.lblhdnProductOrderNumber.Location = new System.Drawing.Point(425, 348);
            this.lblhdnProductOrderNumber.Name = "lblhdnProductOrderNumber";
            this.lblhdnProductOrderNumber.Size = new System.Drawing.Size(217, 20);
            this.lblhdnProductOrderNumber.TabIndex = 32;
            this.lblhdnProductOrderNumber.Text = "Select Product Order Number";
            this.lblhdnProductOrderNumber.Visible = false;
            // 
            // lblhdntarticlecategory
            // 
            this.lblhdntarticlecategory.AutoSize = true;
            this.lblhdntarticlecategory.Location = new System.Drawing.Point(233, 348);
            this.lblhdntarticlecategory.Name = "lblhdntarticlecategory";
            this.lblhdntarticlecategory.Size = new System.Drawing.Size(170, 20);
            this.lblhdntarticlecategory.TabIndex = 31;
            this.lblhdntarticlecategory.Text = "Select Article Category";
            this.lblhdntarticlecategory.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(839, 348);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 32);
            this.button1.TabIndex = 30;
            this.button1.Text = "Delete";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(976, 348);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(100, 32);
            this.btnClear.TabIndex = 29;
            this.btnClear.Text = "Add";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(698, 348);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(100, 32);
            this.btnInsert.TabIndex = 13;
            this.btnInsert.Text = "Save";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // btnDefects
            // 
            this.btnDefects.Location = new System.Drawing.Point(776, 288);
            this.btnDefects.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDefects.Name = "btnDefects";
            this.btnDefects.Size = new System.Drawing.Size(128, 35);
            this.btnDefects.TabIndex = 12;
            this.btnDefects.Text = "Select Defects";
            this.btnDefects.UseVisualStyleBackColor = true;
            this.btnDefects.Click += new System.EventHandler(this.btnDefects_Click);
            // 
            // txtCustomer
            // 
            this.txtCustomer.Location = new System.Drawing.Point(776, 132);
            this.txtCustomer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCustomer.Name = "txtCustomer";
            this.validator1.SetRequiredMessage(this.txtCustomer, "Customer Required.");
            this.txtCustomer.Size = new System.Drawing.Size(300, 26);
            this.txtCustomer.TabIndex = 6;
            this.validator1.SetType(this.txtCustomer, Itboy.Components.ValidationType.Required);
            // 
            // cmbProductOrderNumber
            // 
            this.validator1.SetComparedControl(this.cmbProductOrderNumber, this.lblhdnProductOrderNumber);
            this.validator1.SetCompareMessage(this.cmbProductOrderNumber, "Product Order Number Required.");
            this.validator1.SetCompareOperator(this.cmbProductOrderNumber, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbProductOrderNumber.FormattingEnabled = true;
            this.cmbProductOrderNumber.Location = new System.Drawing.Point(228, 80);
            this.cmbProductOrderNumber.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbProductOrderNumber.Name = "cmbProductOrderNumber";
            this.cmbProductOrderNumber.Size = new System.Drawing.Size(300, 28);
            this.cmbProductOrderNumber.TabIndex = 3;
            this.validator1.SetType(this.cmbProductOrderNumber, Itboy.Components.ValidationType.Compare);
            // 
            // txtApproxDays
            // 
            this.txtApproxDays.Location = new System.Drawing.Point(228, 291);
            this.txtApproxDays.Name = "txtApproxDays";
            this.validator1.SetRequiredMessage(this.txtApproxDays, "Approximate days Required.");
            this.txtApproxDays.Size = new System.Drawing.Size(300, 26);
            this.txtApproxDays.TabIndex = 11;
            this.validator1.SetType(this.txtApproxDays, Itboy.Components.ValidationType.Required);
            // 
            // txtArticlePerMinute
            // 
            this.txtArticlePerMinute.Location = new System.Drawing.Point(776, 237);
            this.txtArticlePerMinute.Name = "txtArticlePerMinute";
            this.validator1.SetRequiredMessage(this.txtArticlePerMinute, "Article Per Minute Required.");
            this.txtArticlePerMinute.Size = new System.Drawing.Size(300, 26);
            this.txtArticlePerMinute.TabIndex = 10;
            this.validator1.SetType(this.txtArticlePerMinute, Itboy.Components.ValidationType.Required);
            // 
            // txtLineSpeed
            // 
            this.txtLineSpeed.Location = new System.Drawing.Point(228, 237);
            this.txtLineSpeed.Name = "txtLineSpeed";
            this.validator1.SetRequiredMessage(this.txtLineSpeed, "Line speed Required.");
            this.txtLineSpeed.Size = new System.Drawing.Size(300, 26);
            this.txtLineSpeed.TabIndex = 9;
            this.validator1.SetType(this.txtLineSpeed, Itboy.Components.ValidationType.Required);
            // 
            // txtEfficiency
            // 
            this.txtEfficiency.Location = new System.Drawing.Point(776, 185);
            this.txtEfficiency.Name = "txtEfficiency";
            this.validator1.SetRequiredMessage(this.txtEfficiency, "Efficiency Required.");
            this.txtEfficiency.Size = new System.Drawing.Size(300, 26);
            this.txtEfficiency.TabIndex = 8;
            this.validator1.SetType(this.txtEfficiency, Itboy.Components.ValidationType.Required);
            // 
            // txtQty
            // 
            this.validator1.SetDataType(this.txtQty, Itboy.Components.ValidationDataType.Integer);
            this.txtQty.Location = new System.Drawing.Point(228, 185);
            this.txtQty.MaxLength = 9;
            this.txtQty.Name = "txtQty";
            this.validator1.SetRangeMessage(this.txtQty, "Qty must be numeric.");
            this.validator1.SetRangeOfLowerBound(this.txtQty, "0");
            this.validator1.SetRangeOfUpperBound(this.txtQty, "999999999");
            this.validator1.SetRequiredMessage(this.txtQty, "Qty Required.");
            this.txtQty.Size = new System.Drawing.Size(300, 26);
            this.txtQty.TabIndex = 7;
            this.validator1.SetType(this.txtQty, ((Itboy.Components.ValidationType)((Itboy.Components.ValidationType.Required | Itboy.Components.ValidationType.Range))));
            // 
            // txtArticleName
            // 
            this.txtArticleName.Location = new System.Drawing.Point(228, 132);
            this.txtArticleName.Name = "txtArticleName";
            this.validator1.SetRequiredMessage(this.txtArticleName, "Article Name Required.");
            this.txtArticleName.Size = new System.Drawing.Size(300, 26);
            this.txtArticleName.TabIndex = 5;
            this.validator1.SetType(this.txtArticleName, Itboy.Components.ValidationType.Required);
            // 
            // txtArticleNumer
            // 
            this.txtArticleNumer.Location = new System.Drawing.Point(776, 80);
            this.txtArticleNumer.Name = "txtArticleNumer";
            this.validator1.SetRequiredMessage(this.txtArticleNumer, "Article Number Required");
            this.txtArticleNumer.Size = new System.Drawing.Size(300, 26);
            this.txtArticleNumer.TabIndex = 4;
            this.validator1.SetType(this.txtArticleNumer, Itboy.Components.ValidationType.Required);
            // 
            // cmbArticleCategory
            // 
            this.validator1.SetComparedControl(this.cmbArticleCategory, this.lblhdntarticlecategory);
            this.validator1.SetCompareMessage(this.cmbArticleCategory, "Article Category Required.");
            this.validator1.SetCompareOperator(this.cmbArticleCategory, Itboy.Components.ValidationCompareOperator.NotEqual);
            this.cmbArticleCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbArticleCategory.FormattingEnabled = true;
            this.cmbArticleCategory.Location = new System.Drawing.Point(776, 28);
            this.cmbArticleCategory.Name = "cmbArticleCategory";
            this.cmbArticleCategory.Size = new System.Drawing.Size(300, 28);
            this.cmbArticleCategory.TabIndex = 2;
            this.validator1.SetType(this.cmbArticleCategory, Itboy.Components.ValidationType.Compare);
            this.cmbArticleCategory.SelectedIndexChanged += new System.EventHandler(this.cmbArticleCategory_SelectedIndexChanged);
            // 
            // txtLineNumber
            // 
            this.txtLineNumber.Location = new System.Drawing.Point(228, 28);
            this.txtLineNumber.Name = "txtLineNumber";
            this.validator1.SetRequiredMessage(this.txtLineNumber, "Line Number Required.");
            this.txtLineNumber.Size = new System.Drawing.Size(300, 26);
            this.txtLineNumber.TabIndex = 1;
            this.validator1.SetType(this.txtLineNumber, Itboy.Components.ValidationType.Required);
            // 
            // lblDefectSelection
            // 
            this.lblDefectSelection.AutoSize = true;
            this.lblDefectSelection.Location = new System.Drawing.Point(610, 295);
            this.lblDefectSelection.Name = "lblDefectSelection";
            this.lblDefectSelection.Size = new System.Drawing.Size(135, 20);
            this.lblDefectSelection.TabIndex = 12;
            this.lblDefectSelection.Text = "Defect Selection :";
            // 
            // lblApproxDay
            // 
            this.lblApproxDay.AutoSize = true;
            this.lblApproxDay.Location = new System.Drawing.Point(38, 295);
            this.lblApproxDay.Name = "lblApproxDay";
            this.lblApproxDay.Size = new System.Drawing.Size(149, 20);
            this.lblApproxDay.TabIndex = 11;
            this.lblApproxDay.Text = "Approx days of run :";
            // 
            // lblArticlePerMinute
            // 
            this.lblArticlePerMinute.AutoSize = true;
            this.lblArticlePerMinute.Location = new System.Drawing.Point(604, 242);
            this.lblArticlePerMinute.Name = "lblArticlePerMinute";
            this.lblArticlePerMinute.Size = new System.Drawing.Size(141, 20);
            this.lblArticlePerMinute.TabIndex = 10;
            this.lblArticlePerMinute.Text = "Article Per Minute :";
            // 
            // lblLineSpeed
            // 
            this.lblLineSpeed.AutoSize = true;
            this.lblLineSpeed.Location = new System.Drawing.Point(88, 242);
            this.lblLineSpeed.Name = "lblLineSpeed";
            this.lblLineSpeed.Size = new System.Drawing.Size(98, 20);
            this.lblLineSpeed.TabIndex = 9;
            this.lblLineSpeed.Text = "Line Speed :";
            // 
            // lblEfficiency
            // 
            this.lblEfficiency.AutoSize = true;
            this.lblEfficiency.Location = new System.Drawing.Point(660, 189);
            this.lblEfficiency.Name = "lblEfficiency";
            this.lblEfficiency.Size = new System.Drawing.Size(85, 20);
            this.lblEfficiency.TabIndex = 8;
            this.lblEfficiency.Text = "Efficiency :";
            // 
            // lblQty
            // 
            this.lblQty.AutoSize = true;
            this.lblQty.Location = new System.Drawing.Point(146, 189);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(41, 20);
            this.lblQty.TabIndex = 7;
            this.lblQty.Text = "Qty :";
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Location = new System.Drawing.Point(663, 137);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(86, 20);
            this.lblCustomer.TabIndex = 6;
            this.lblCustomer.Text = "Customer :";
            // 
            // lblArticleName
            // 
            this.lblArticleName.AutoSize = true;
            this.lblArticleName.Location = new System.Drawing.Point(88, 137);
            this.lblArticleName.Name = "lblArticleName";
            this.lblArticleName.Size = new System.Drawing.Size(107, 20);
            this.lblArticleName.TabIndex = 5;
            this.lblArticleName.Text = "Article Name :";
            // 
            // lblArticleNumber
            // 
            this.lblArticleNumber.AutoSize = true;
            this.lblArticleNumber.Location = new System.Drawing.Point(634, 85);
            this.lblArticleNumber.Name = "lblArticleNumber";
            this.lblArticleNumber.Size = new System.Drawing.Size(112, 20);
            this.lblArticleNumber.TabIndex = 4;
            this.lblArticleNumber.Text = "Article Numer :";
            // 
            // lblProdOrderNo
            // 
            this.lblProdOrderNo.AutoSize = true;
            this.lblProdOrderNo.Location = new System.Drawing.Point(3, 85);
            this.lblProdOrderNo.Name = "lblProdOrderNo";
            this.lblProdOrderNo.Size = new System.Drawing.Size(197, 20);
            this.lblProdOrderNo.TabIndex = 3;
            this.lblProdOrderNo.Text = "Production Order Number :";
            // 
            // lblArticleCategory
            // 
            this.lblArticleCategory.AutoSize = true;
            this.lblArticleCategory.Location = new System.Drawing.Point(618, 32);
            this.lblArticleCategory.Name = "lblArticleCategory";
            this.lblArticleCategory.Size = new System.Drawing.Size(129, 20);
            this.lblArticleCategory.TabIndex = 2;
            this.lblArticleCategory.Text = "Article Category :";
            // 
            // lblLineNumber
            // 
            this.lblLineNumber.AutoSize = true;
            this.lblLineNumber.Location = new System.Drawing.Point(88, 32);
            this.lblLineNumber.Name = "lblLineNumber";
            this.lblLineNumber.Size = new System.Drawing.Size(107, 20);
            this.lblLineNumber.TabIndex = 1;
            this.lblLineNumber.Text = "Line Number :";
            // 
            // validator1
            // 
            this.validator1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.validator1.Form = this;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Location = new System.Drawing.Point(11, 412);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1122, 256);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Jobs";
            // 
            // dataGridView1
            // 
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NewJobId,
            this.LineNumber,
            this.ArticleCategory,
            this.ProductionOrderNumber,
            this.ArticleNumber,
            this.ArticleName,
            this.Customer,
            this.Qty,
            this.Efficiency,
            this.LineSpeed,
            this.ArticlePerMinute,
            this.ApproxDays,
            this.DefectIds,
            this.DefectName});
            this.dataGridView1.Location = new System.Drawing.Point(8, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(1103, 225);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_RowHeaderMouseClick_1);
            // 
            // NewJobId
            // 
            this.NewJobId.DataPropertyName = "NewJobId";
            this.NewJobId.HeaderText = "NewJobId";
            this.NewJobId.Name = "NewJobId";
            this.NewJobId.ReadOnly = true;
            this.NewJobId.Visible = false;
            this.NewJobId.Width = 5;
            // 
            // LineNumber
            // 
            this.LineNumber.DataPropertyName = "LineNumber";
            this.LineNumber.HeaderText = "Line Number";
            this.LineNumber.Name = "LineNumber";
            this.LineNumber.ReadOnly = true;
            // 
            // ArticleCategory
            // 
            this.ArticleCategory.DataPropertyName = "ArticleCategory";
            this.ArticleCategory.HeaderText = "Article Category";
            this.ArticleCategory.Name = "ArticleCategory";
            this.ArticleCategory.ReadOnly = true;
            this.ArticleCategory.Visible = false;
            this.ArticleCategory.Width = 5;
            // 
            // ProductionOrderNumber
            // 
            this.ProductionOrderNumber.DataPropertyName = "ProductionOrderNumber";
            this.ProductionOrderNumber.HeaderText = "Production Order Number";
            this.ProductionOrderNumber.Name = "ProductionOrderNumber";
            this.ProductionOrderNumber.ReadOnly = true;
            this.ProductionOrderNumber.Width = 134;
            // 
            // ArticleNumber
            // 
            this.ArticleNumber.DataPropertyName = "ArticleNumber";
            this.ArticleNumber.HeaderText = "Article Number";
            this.ArticleNumber.Name = "ArticleNumber";
            this.ArticleNumber.ReadOnly = true;
            // 
            // ArticleName
            // 
            this.ArticleName.DataPropertyName = "ArticleName";
            this.ArticleName.HeaderText = "Article Name";
            this.ArticleName.Name = "ArticleName";
            this.ArticleName.ReadOnly = true;
            // 
            // Customer
            // 
            this.Customer.DataPropertyName = "Customer";
            this.Customer.HeaderText = "Customer";
            this.Customer.Name = "Customer";
            this.Customer.ReadOnly = true;
            this.Customer.Visible = false;
            this.Customer.Width = 5;
            // 
            // Qty
            // 
            this.Qty.DataPropertyName = "Qty";
            this.Qty.HeaderText = "Qty";
            this.Qty.Name = "Qty";
            this.Qty.ReadOnly = true;
            this.Qty.Visible = false;
            this.Qty.Width = 5;
            // 
            // Efficiency
            // 
            this.Efficiency.DataPropertyName = "Efficiency";
            this.Efficiency.HeaderText = "Efficiency";
            this.Efficiency.Name = "Efficiency";
            this.Efficiency.ReadOnly = true;
            this.Efficiency.Visible = false;
            this.Efficiency.Width = 5;
            // 
            // LineSpeed
            // 
            this.LineSpeed.DataPropertyName = "LineSpeed";
            this.LineSpeed.HeaderText = "Line Speed";
            this.LineSpeed.Name = "LineSpeed";
            this.LineSpeed.ReadOnly = true;
            this.LineSpeed.Visible = false;
            this.LineSpeed.Width = 116;
            // 
            // ArticlePerMinute
            // 
            this.ArticlePerMinute.DataPropertyName = "ArticlePerMinute";
            this.ArticlePerMinute.HeaderText = "Article Per Minute";
            this.ArticlePerMinute.Name = "ArticlePerMinute";
            this.ArticlePerMinute.ReadOnly = true;
            this.ArticlePerMinute.Visible = false;
            this.ArticlePerMinute.Width = 112;
            // 
            // ApproxDays
            // 
            this.ApproxDays.DataPropertyName = "ApproxDays";
            this.ApproxDays.HeaderText = "Approx Days";
            this.ApproxDays.Name = "ApproxDays";
            this.ApproxDays.ReadOnly = true;
            this.ApproxDays.Visible = false;
            this.ApproxDays.Width = 125;
            // 
            // DefectIds
            // 
            this.DefectIds.DataPropertyName = "DefectIds";
            this.DefectIds.HeaderText = "DefectIds";
            this.DefectIds.Name = "DefectIds";
            this.DefectIds.ReadOnly = true;
            this.DefectIds.Visible = false;
            // 
            // DefectName
            // 
            this.DefectName.DataPropertyName = "DefectName";
            this.DefectName.HeaderText = "Defects";
            this.DefectName.Name = "DefectName";
            this.DefectName.ReadOnly = true;
            this.DefectName.Width = 250;
            // 
            // SetUpNewJob
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1145, 694);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetUpNewJob";
            this.Text = "SetUpNewJob";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblQty;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.Label lblArticleName;
        private System.Windows.Forms.Label lblArticleNumber;
        private System.Windows.Forms.Label lblProdOrderNo;
        private System.Windows.Forms.Label lblArticleCategory;
        private System.Windows.Forms.Label lblLineNumber;
        private System.Windows.Forms.Label lblEfficiency;
        private System.Windows.Forms.Label lblArticlePerMinute;
        private System.Windows.Forms.Label lblLineSpeed;
        private System.Windows.Forms.Label lblApproxDay;
        private System.Windows.Forms.Label lblDefectSelection;
        private System.Windows.Forms.TextBox txtLineNumber;
        private System.Windows.Forms.ComboBox cmbArticleCategory;
        private System.Windows.Forms.TextBox txtArticleNumer;
        private System.Windows.Forms.TextBox txtArticleName;
        private System.Windows.Forms.TextBox txtQty;
        private System.Windows.Forms.TextBox txtEfficiency;
        private System.Windows.Forms.TextBox txtLineSpeed;
        private System.Windows.Forms.TextBox txtArticlePerMinute;
        private System.Windows.Forms.TextBox txtApproxDays;
        private System.Windows.Forms.ComboBox cmbProductOrderNumber;
        private System.Windows.Forms.TextBox txtCustomer;
        private System.Windows.Forms.Button btnDefects;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnInsert;
        private Itboy.Components.Validator validator1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblhdnProductOrderNumber;
        private System.Windows.Forms.Label lblhdntarticlecategory;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn NewJobId;
        private System.Windows.Forms.DataGridViewTextBoxColumn LineNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArticleCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductionOrderNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArticleNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArticleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Customer;
        private System.Windows.Forms.DataGridViewTextBoxColumn Qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn Efficiency;
        private System.Windows.Forms.DataGridViewTextBoxColumn LineSpeed;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArticlePerMinute;
        private System.Windows.Forms.DataGridViewTextBoxColumn ApproxDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn DefectIds;
        private System.Windows.Forms.DataGridViewTextBoxColumn DefectName;
    }
}