﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmMonitoringSystem
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            panel1.AllowDrop = true;
            panel2.AllowDrop = true;
            panel3.AllowDrop = true;
            panel4.AllowDrop = true;
            panel5.AllowDrop = true;
            panel6.AllowDrop = true;
            panel7.AllowDrop = true;

            panel1.DragEnter += panel_DragEnter;
            panel2.DragEnter += panel_DragEnter;
            panel3.DragEnter += panel_DragEnter;
            panel4.DragEnter += panel_DragEnter;
            panel5.DragEnter += panel_DragEnter;
            panel6.DragEnter += panel_DragEnter;
            panel7.DragEnter += panel_DragEnter;

            panel1.DragDrop += panel_DragDrop;
            panel2.DragDrop += panel_DragDrop;
            panel3.DragDrop += panel_DragDrop;
            panel4.DragDrop += panel_DragDrop;
            panel5.DragDrop += panel_DragDrop;
            panel6.DragDrop += panel_DragDrop;
            panel7.DragDrop += panel_DragDrop;

            panel8.MouseDown += button1_MouseDown;
            panel9.MouseDown += button1_MouseDown;
            panel10.MouseDown += button1_MouseDown;
            panel11.MouseDown += button1_MouseDown;
            panel12.MouseDown += button1_MouseDown;
            panel13.MouseDown += button1_MouseDown;
            panel14.MouseDown += button1_MouseDown;
        }

        void button1_MouseDown(object sender, MouseEventArgs e)
        {
            ((Panel)sender).DoDragDrop(((Panel)sender), DragDropEffects.Move);
        }

        void panel_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        void panel_DragDrop(object sender, DragEventArgs e)
        {
            ((Panel)sender).Controls[0].Parent = ((Panel)e.Data.GetData(typeof(Panel))).Parent;
            ((Panel)e.Data.GetData(typeof(Panel))).Parent = (Panel)sender;
           // ((Panel)sender).Controls.Add(((Panel)e.Data.GetData(typeof(Panel))).Parent.Controls[0]);
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
