﻿using AlarmMonitoringClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmMonitoringSystem
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public string ImageToBase64(Image image,
             System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        public Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.Text, "update defectmaster set image='" + ImageToBase64(pictureBox1.Image, System.Drawing.Imaging.ImageFormat.Png) + "' where DefectId=1");
            SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.Text, "update defectmaster set image='" + ImageToBase64(pictureBox2.Image, System.Drawing.Imaging.ImageFormat.Png) + "' where DefectId=2");

            SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.Text, "update defectmaster set image='" + ImageToBase64(pictureBox3.Image, System.Drawing.Imaging.ImageFormat.Png) + "' where DefectId=3");

            SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.Text, "update defectmaster set image='" + ImageToBase64(pictureBox4.Image, System.Drawing.Imaging.ImageFormat.Png) + "' where DefectId=4");

            SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.Text, "update defectmaster set image='" + ImageToBase64(pictureBox5.Image, System.Drawing.Imaging.ImageFormat.Png) + "' where DefectId=5");

            SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.Text, "update defectmaster set image='" + ImageToBase64(pictureBox6.Image, System.Drawing.Imaging.ImageFormat.Png) + "' where DefectId=6");

            SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.Text, "update defectmaster set image='" + ImageToBase64(pictureBox7.Image, System.Drawing.Imaging.ImageFormat.Png) + "' where DefectId=7");

            SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.Text, "update defectmaster set image='" + ImageToBase64(pictureBox8.Image, System.Drawing.Imaging.ImageFormat.Png) + "' where DefectId=8");
            SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.Text, "update defectmaster set image='" + ImageToBase64(pictureBox9.Image, System.Drawing.Imaging.ImageFormat.Png) + "' where DefectId=9");
            SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.Text, "update defectmaster set image='" + ImageToBase64(pictureBox10.Image, System.Drawing.Imaging.ImageFormat.Png) + "' where DefectId=10");
            SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.Text, "update defectmaster set image='" + ImageToBase64(pictureBox11.Image, System.Drawing.Imaging.ImageFormat.Png) + "' where DefectId=11");
            SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.Text, "update defectmaster set image='" + ImageToBase64(pictureBox12.Image, System.Drawing.Imaging.ImageFormat.Png) + "' where DefectId=12");

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {

        }
    }
}
