﻿using AlarmMonitoringSystem.Admin;
using AlarmMonitoringSystem.ColdendForeman;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmMonitoringSystem
{
    public partial class Login : Form
    {
        RegistrationBAL obj = new RegistrationBAL();
        static DataTable tbUsers = new DataTable();
        public Login()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            BindLoginType();
        }

        private void BindLoginType()
        {
            cmbLoginType.ValueMember = "LoginTypeId";
            cmbLoginType.DisplayMember = "LoginType";
            cmbLoginType.DataSource = obj.GetLoginTypes();
        }

        private void cmbLoginType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbUsername.ValueMember = "UserId";
            cmbUsername.DisplayMember = "Username";
            tbUsers = obj.GetUsersByLoginTypes(Convert.ToInt32(cmbLoginType.SelectedValue.ToString()));
            cmbUsername.DataSource = tbUsers;
        }

       

        private void cmbUsername_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow[] dr = tbUsers.Select("UserId=" + Convert.ToInt64(cmbUsername.SelectedValue.ToString()));
            txtFileNumber.Text = (dr.Length > 0 ? dr[0]["FileNumber"].ToString() : string.Empty);
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (validator1.Validate())
            {
                DataRow[] dr = tbUsers.Select("UserId=" + Convert.ToInt64(cmbUsername.SelectedValue.ToString()));
                string password = (dr.Length > 0 ? dr[0]["Password"].ToString() : string.Empty);
                if (Encrypt.DecryptString(password) == txtPassword.Text)
                {
                    UserDetail userInfo = new UserDetail(cmbUsername.Text, cmbLoginType.Text, Convert.ToInt64(cmbUsername.SelectedValue.ToString()));
                    this.Hide();
                    if (cmbLoginType.Text == "Admin")
                    {
                        var admin = new MDIParentAdmin();
                        admin.ShowDialog();
                    }
                    else if (cmbLoginType.Text == "Coldend Sorter")
                    {
                        var jobs = new Jobs();
                        jobs.ShowDialog();
                    }
                    this.Show();
                }
                else
                {
                    CommonWeb.Alert("Incorrect Password.", "error");
                }
            }
        }
    }
}
