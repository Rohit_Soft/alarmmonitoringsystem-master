﻿using AlarmMonitoringClassLibrary.AlarmMonitoringDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmMonitoringClassLibrary.AlarmMonitoringBAL
{
    public class SectionBAL
    {
        public int SectionId { get; set; }
        public string Section { get; set; }

        public int DepartmentId { get; set; }

        SectionDAL objDAL = new SectionDAL();

        public DataTable GetAllSections()
        {
            DataTable dt = objDAL.GetAllSections();
            return dt;
        }

        public int InsertUpdateSections(SectionBAL obj)
        {
            return objDAL.InsertUpdateSections(obj);
        }

        public int DeleteSection(int sectionId)
        {
            return objDAL.DeleteSection(sectionId);
        }

        public DataTable GetAllSectionsByDepartment(int departmentId)
        {
            DataTable dt = objDAL.GetAllSectionsByDepartment(departmentId);
            DataRow dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "Select Section"};
            dt.Rows.InsertAt(dr, 0);
            return dt;
        }
    }
}
