﻿using AlarmMonitoringClassLibrary.AlarmMonitoringDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmMonitoringClassLibrary.AlarmMonitoringBAL
{
    public class EmployeeBAL
    {
        public long EmployeeId { get; set; }
        public int NationalityId { get; set; }
        public int DepartmentId { get; set; }
        public int SectionId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FileNumber { get; set; }
        public string EmployeeCode { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int DesignationId { get; set; }
        public long ManagerId { get; set; }
        public int LoginTypeId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public long CreatedBy { get; set; }

        EmployeeDAL objDAL = new EmployeeDAL();
        public DataTable GetAllDepartments(bool flag)
        {
            DataTable dt = objDAL.GetAllEmployees();
            if (flag)
            {
                DataRow dr = dt.NewRow();
                dr.ItemArray = new object[] { 0, "Select Employee" };
                dt.Rows.InsertAt(dr, 0);
            }
            return dt;
        }

        public DataTable GetAllNationality()
        {
            DataTable dt = objDAL.GetAllNationality();
            DataRow dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "Select Nationality" };
            dt.Rows.InsertAt(dr, 0);
            return dt;
        }

        public int InsertUpdateEmployee(EmployeeBAL obj)
        {
            return objDAL.InsertUpdateEmployee(obj);
        }

        public int DeleteEmployee(EmployeeBAL obj)
        {
            return objDAL.DeleteEmployee(obj);
        }

        public string GetFileNumberByEmployee(long employeeId)
        {
            return objDAL.GetFileNumberByEmployee(employeeId);
        }

        public DataTable GetAllDesignation()
        {
            DataTable dt = objDAL.GetAllDesignation();
            DataRow dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "Select Designation" };
            dt.Rows.InsertAt(dr, 0);
            return dt;
        }

        public DataTable GetAllManagers(long employeeId)
        {
            DataTable dt = objDAL.GetAllManagers(employeeId);
            DataRow dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "Select Manager" };
            dt.Rows.InsertAt(dr, 0);
            return dt;
        }

        public string GetEmailByEmployee(Int64 employeeId)
        {
            return objDAL.GetEmailByEmployee(employeeId);
        }
        }
}