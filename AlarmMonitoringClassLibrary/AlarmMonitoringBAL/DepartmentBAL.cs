﻿using AlarmMonitoringClassLibrary.AlarmMonitoringDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmMonitoringClassLibrary.AlarmMonitoringBAL
{
   public class DepartmentBAL
    {
        public int DepartmentId { get; set; }
        public string Department { get; set; }

        DepartmentDAL objDAL = new DepartmentDAL();
        public DataTable GetAllDepartments(bool flag)
        {
            DataTable dt = objDAL.GetAllDepartments();
            if (flag)
            {
                DataRow dr = dt.NewRow();
                dr.ItemArray = new object[] { 0, "Select Department",0 };
                dt.Rows.InsertAt(dr, 0);
            }
            return dt;
        }
        public int InsertUpdateDepartments(DepartmentBAL obj)
        {
            return objDAL.InsertUpdateDepartments(obj);
        }
        public int DeleteDepartment(int departmentId)
        {
            return objDAL.DeleteDepartment(departmentId);
        }
    }
}
