﻿using AlarmMonitoringClassLibrary.AlarmMonitoringDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmMonitoringClassLibrary.AlarmMonitoringBAL
{
    public class SetupNewJobBAL
    {
        public int NewJobId { get; set; }
        public string LineNumber { get; set; }
        public int ArticleCategory { get; set; }
        public int ProductionOrderId { get; set; }
        public string ArticleNumber { get; set; }
        public string ArticleName { get; set; }
        public string Customer { get; set; }
        public int Qty { get; set; }
        public string Efficiency { get; set; }
        public string LineSpeed { get; set; }
        public string ArticlePerMinute { get; set; }
        public string ApproxDays { get; set; }
        public string DefectSelection { get; set; }

        SetupNewJobDAL objDAL = new SetupNewJobDAL();

        public int InsertUpdateSetupNewJob(SetupNewJobBAL obj)
        {
            return objDAL.InsertUpdateSetupNewJob(obj);
        }
        public DataTable AutoCompleterProductOrderNumber(int ArticleCategoryId)
        {
            DataTable dt = objDAL.AutoCompleterProductOrderNumber(ArticleCategoryId);
            DataRow dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "Select Product Order Number" };
            dt.Rows.InsertAt(dr, 0);
            return dt;
        }
        public DataTable GetAllArticleCategory()
        {
            DataTable dt = objDAL.GetAllArticleCategory();
            DataRow dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "Select Article Category" };
            dt.Rows.InsertAt(dr, 0);
            return dt;
        }

        public DataTable GetAllSetupJobs()
        {
            return objDAL.GetAllSetupJobs();
        }

        public int DeleteSetupNewJob(Int64 NewJobId)
        {
            return objDAL.DeleteSetupNewJob(NewJobId);
        }
    }
}
