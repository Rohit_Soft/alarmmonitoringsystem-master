﻿using System.Data;

namespace AlarmMonitoringSystem
{
    public class RegistrationBAL
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public int LoginTypeId { get; set; }
        public string Username { get; set; }
        public string FileNumber { get; set; }
        public string Password { get; set; }

        RegistrationDAL objDAL = new RegistrationDAL();
        public DataTable GetLoginTypes()
        {
            DataTable dt = objDAL.GetLoginTypes();
            DataRow dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "Select Login Type" };
            dt.Rows.InsertAt(dr, 0);
            return dt;
        }

        public int RegisterUser(RegistrationBAL obj)
        {
            return objDAL.RegisterUser(obj);
        }

        public DataTable GetUsersByLoginTypes(int loginTypeId)
        {
            DataTable dt = objDAL.GetUsersByLoginTypes(loginTypeId);
            DataRow dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "Select User" };
            dt.Rows.InsertAt(dr, 0);
            return dt;
        }

        public DataTable GetAllNames(int departmentId,int SectionId,int NationalityId)
        {
            DataTable dt = objDAL.GetAllNames(departmentId,SectionId,NationalityId);
            DataRow dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "Select Name" };
            dt.Rows.InsertAt(dr, 0);
            return dt;
        }
    }
}
