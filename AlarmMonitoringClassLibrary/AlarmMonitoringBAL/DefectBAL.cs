﻿using AlarmMonitoringClassLibrary.AlarmMonitoringDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmMonitoringClassLibrary.AlarmMonitoringBAL
{
    public class DefectBAL
    {
        public int DefectId { get; set; }
        public string DefectName { get; set; }
        public string DefectCategory { get; set; }
        public string Image { get; set; }

        DefectDAL objDAL = new DefectDAL();

        public DataTable GetAllDefects(string type = "")
        {
            DataTable dt = objDAL.GetAllDefects(type);
            return dt;
        }

        public DataTable GetAllDefectsColdend(long id)
        {
            DataTable dt = objDAL.GetAllDefectsColdend(id);
            return dt;
        }

        public DataTable GetAllDefectsColdendForRemove( long id)
        {
            DataTable dt = objDAL.GetAllDefectsColdendForRemove(id);
            return dt;
        }

        public int InsertUpdateDefect(DefectBAL obj)
        {
            return objDAL.InsertUpdateDefect(obj);
        }

        public int DeleteDefect(int defectId)
        {
            return objDAL.DeleteDefect(defectId);
        }

        public void UpdateDefectOrder(DataTable tb, long id)
        {
            objDAL.UpdateDefectOrder(tb, id);

        }

        public void UpdateDefectColdend(long id, bool deleted)
        {
            objDAL.UpdateDefectColdend(id, deleted);
        }
    }
}
