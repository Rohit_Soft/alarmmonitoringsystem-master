﻿using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmMonitoringClassLibrary.AlarmMonitoringDAL
{
    public class DepartmentDAL
    {
        public DataTable GetAllDepartments()
        {
            try
            {
                return SqlHelper.ExecuteDataset(Common.DBConnection, CommandType.StoredProcedure, "GetAllDepartments").Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public int InsertUpdateDepartments(DepartmentBAL obj)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@DepartmentId", obj.DepartmentId);
                param[1] = new SqlParameter("@Department", obj.Department);
                return Convert.ToInt32(SqlHelper.ExecuteScalar(Common.DBConnection, CommandType.StoredProcedure, "InsertUpdateDepartment", param));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public int DeleteDepartment(int departmentId)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@DepartmentId", departmentId);
                return Convert.ToInt32(SqlHelper.ExecuteScalar(Common.DBConnection, CommandType.StoredProcedure, "DeleteDepartment", param));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}
