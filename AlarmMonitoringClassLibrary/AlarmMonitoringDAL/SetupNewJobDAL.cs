﻿using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmMonitoringClassLibrary.AlarmMonitoringDAL
{
    public class SetupNewJobDAL
    {
        public int InsertUpdateSetupNewJob(SetupNewJobBAL obj)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[13];
                param[0] = new SqlParameter("@NewJobId", obj.NewJobId);
                param[1] = new SqlParameter("@LineNumber", obj.LineNumber);
                param[2] = new SqlParameter("@ArticleCategory", obj.ArticleCategory);
                param[3] = new SqlParameter("@ProductOrderId", obj.ProductionOrderId);
                param[4] = new SqlParameter("@ArticleNumber", obj.ArticleNumber);
                param[5] = new SqlParameter("@ArticleName", obj.ArticleName);
                param[6] = new SqlParameter("@Customer", obj.Customer);
                param[7] = new SqlParameter("@Qty", obj.Qty);
                param[8] = new SqlParameter("@Efficiency", obj.Efficiency);
                param[9] = new SqlParameter("@LineSpeed", obj.LineSpeed);
                param[10] = new SqlParameter("@ArticlePerMinute", obj.ArticlePerMinute);
                param[11] = new SqlParameter("@ApproxDays", obj.ApproxDays);
                param[12] = new SqlParameter("@DefectSelection", obj.DefectSelection);

                return Convert.ToInt32(SqlHelper.ExecuteScalar(Common.DBConnection, CommandType.StoredProcedure, "InsertUpdateSetupNewJob", param));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public DataTable AutoCompleterProductOrderNumber(int ArticleCategoryId)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ArticleCategoryId", ArticleCategoryId);
                return SqlHelper.ExecuteDataset(Common.DBConnection, CommandType.StoredProcedure, "GetProductOrderNumberByArticleId", param).Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public DataTable GetAllArticleCategory()
        {
            try
            {
                return SqlHelper.ExecuteDataset(Common.DBConnection, CommandType.StoredProcedure, "GetAllArticleCategory").Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public DataTable GetAllSetupJobs()
        {
            try
            {
                return SqlHelper.ExecuteDataset(Common.DBConnection, CommandType.StoredProcedure, "GetAllSetupJobs").Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public int DeleteSetupNewJob(Int64 NewJobId)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@NewJobId", NewJobId);
                return SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.StoredProcedure, "DeleteSetupNewJob", param);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}
