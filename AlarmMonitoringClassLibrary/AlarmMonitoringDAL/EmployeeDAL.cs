﻿using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmMonitoringClassLibrary.AlarmMonitoringDAL
{
    class EmployeeDAL
    {
        public DataTable GetAllEmployees()
        {
            try
            {
                return SqlHelper.ExecuteDataset(Common.DBConnection, CommandType.StoredProcedure, "GetAllEmployees").Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public DataTable GetAllNationality()
        {
            try
            {
                return SqlHelper.ExecuteDataset(Common.DBConnection, CommandType.StoredProcedure, "GetAllNations").Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public int InsertUpdateEmployee(EmployeeBAL obj)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[16];
                param[0] = new SqlParameter("@EmployeeId", obj.EmployeeId);
                param[1] = new SqlParameter("@NationalityId", obj.NationalityId);
                param[2] = new SqlParameter("@DepartmentId", obj.DepartmentId);
                param[3] = new SqlParameter("@SectionId", obj.SectionId);
                param[4] = new SqlParameter("@FirstName", obj.FirstName);
                param[5] = new SqlParameter("@LastName", obj.LastName);
                param[6] = new SqlParameter("@FileNumber",obj.FileNumber);
                param[7] = new SqlParameter("@EmployeeCode",obj.EmployeeCode);
                param[8] = new SqlParameter("@Email",obj.Email);
                param[9] = new SqlParameter("@Phone",obj.Phone);
                param[10] = new SqlParameter("@DesignationId",obj.DesignationId);
                param[11] = new SqlParameter("@ManagerId",obj.ManagerId);
                param[12] = new SqlParameter("@LoginTypeId",obj.LoginTypeId);
                param[13] = new SqlParameter("@Username",obj.UserName);
                param[14] = new SqlParameter("@Password",obj.Password);
                param[15] = new SqlParameter("@CreatedBy",obj.CreatedBy);
                return Convert.ToInt32(SqlHelper.ExecuteScalar(Common.DBConnection, CommandType.StoredProcedure, "InsertUpdateEmployee", param));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public int DeleteEmployee(EmployeeBAL obj)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@EmployeeId", obj.EmployeeId);
                return SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.StoredProcedure, "DeleteEmployee", param);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public string GetFileNumberByEmployee(long employeeId)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@EmployeeId", employeeId);
                return Convert.ToString(SqlHelper.ExecuteScalar(Common.DBConnection, CommandType.StoredProcedure, "GetEmployeeFileNumberById", param));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public DataTable GetAllDesignation()
        {
            try
            {
                return SqlHelper.ExecuteDataset(Common.DBConnection, CommandType.StoredProcedure, "GetAllDesignatons").Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public DataTable GetAllManagers(long employeeId)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@EmployeeId", employeeId);
                return SqlHelper.ExecuteDataset(Common.DBConnection, CommandType.StoredProcedure, "GetAllManagers", param).Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public string GetEmailByEmployee(Int64 employeeId)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@EmployeeId", employeeId);
                return Convert.ToString(SqlHelper.ExecuteScalar(Common.DBConnection, CommandType.StoredProcedure, "GetEmailByEmployee", param));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}
