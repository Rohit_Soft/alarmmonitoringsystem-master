﻿using AlarmMonitoringClassLibrary.AlarmMonitoringBAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmMonitoringClassLibrary.AlarmMonitoringDAL
{
    public class DefectDAL
    {
        public DataTable GetAllDefects(string type = "")
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@Type", type);
                return SqlHelper.ExecuteDataset(Common.DBConnection, CommandType.StoredProcedure, "GetAllDefects", param).Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public DataTable GetAllDefectsColdend( long id)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@Id", id);
                return SqlHelper.ExecuteDataset(Common.DBConnection, CommandType.StoredProcedure, "GetAllDefectsColdend", param).Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public DataTable GetAllDefectsColdendForRemove(long id)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@Id", id);
                return SqlHelper.ExecuteDataset(Common.DBConnection, CommandType.StoredProcedure, "GetAllDefectsColdendForRemove", param).Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public void UpdateDefectColdend(long id,bool deleted )
        {
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@Id", id);
                param[1] = new SqlParameter("@IsDeleted", deleted);
              //  param[2] = new SqlParameter("@Category", category);
                
                 SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.StoredProcedure, "UpdateDefectsColdend", param);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public int InsertUpdateDefect(DefectBAL obj)
        {
            try
            {

                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter("@DefectId", obj.DefectId);
                param[1] = new SqlParameter("@DefectName", obj.DefectName);
                param[2] = new SqlParameter("@DefectCategory", obj.DefectCategory);
                param[3] = new SqlParameter("@Image", obj.Image);
                return Convert.ToInt32(SqlHelper.ExecuteScalar(Common.DBConnection, CommandType.StoredProcedure, "InsertUpdateDefect", param));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public int DeleteDefect(int defectId)
        {
            try
            {

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@DefectId", defectId);
                return Convert.ToInt32(SqlHelper.ExecuteScalar(Common.DBConnection, CommandType.StoredProcedure, "DeleteDefect", param));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public void UpdateDefectOrder(DataTable tb,long id )
        {
            try
            {

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@Id", id);
                param[1] = new SqlParameter("@Table", tb);
                SqlHelper.ExecuteNonQuery(Common.DBConnection, CommandType.StoredProcedure, "UpdateDefectOrder", param);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}
