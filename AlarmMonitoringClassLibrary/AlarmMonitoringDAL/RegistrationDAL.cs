﻿using AlarmMonitoringClassLibrary;
using System;
using System.Data;
using System.Data.SqlClient;

namespace AlarmMonitoringSystem
{
    public class RegistrationDAL
    {
        public DataTable GetLoginTypes()
        {
            try
            {
                return SqlHelper.ExecuteDataset(Common.DBConnection, CommandType.StoredProcedure, "GetLoginTypes").Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public int RegisterUser(RegistrationBAL obj)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[6];
                param[0] = new SqlParameter("@Name", obj.Name);
                param[1] = new SqlParameter("@LoginTypeId", obj.LoginTypeId);
                param[2] = new SqlParameter("@Username", obj.Username);
                param[3] = new SqlParameter("@FileNumber", obj.FileNumber);
                param[4] = new SqlParameter("@Password", obj.Password);
                param[5] = new SqlParameter("@EmployeeId", obj.EmployeeId);
                return Convert.ToInt32(SqlHelper.ExecuteScalar(Common.DBConnection, CommandType.StoredProcedure, "RegisterUser", param));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public DataTable GetUsersByLoginTypes(int loginTypeId)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@LoginTypeId", loginTypeId);
                return SqlHelper.ExecuteDataset(Common.DBConnection, CommandType.StoredProcedure, "GetUsersByLoginType", param).Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public DataTable GetAllNames(int departmentId, int SectionId, int NationalityId)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@DepartmentId", departmentId);
                param[1] = new SqlParameter("@SectionId", SectionId);
                param[2] = new SqlParameter("@NationalityId", NationalityId);
                return SqlHelper.ExecuteDataset(Common.DBConnection, CommandType.StoredProcedure, "GetAllNames",param).Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

    }
}
